This repository hosts code developed as part of the course 'Computational Fluid Dynamics' (ME 614) taught during spring 2015 semester. 

**NOTE**: To run a hw file from the terminal access the code from the 'me614_cfd_spring2015' folder by typing in the following command on the terminal: 

$python homework0/code/homework.py

## **CONTENTS:** 

**1.** *Homework 0* 

This homework is a basic tutorial in generating a sine and cosine curve using Python. 

