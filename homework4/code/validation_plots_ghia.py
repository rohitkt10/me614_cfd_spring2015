#!/usr/bin/env python

import os
import sys
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime
import argparse
from pdb import set_trace as keyboard

#switches
test = False

#define folder to save plots 
figure_folder = "../report/figures/"

#create folder 
if not os.path.exists(figure_folder):
    os.makedirs(figure_folder)

#store npy files
npy_store_path = './data/'
if not os.path.exists(npy_store_path):
    os.makedirs(npy_store_path)

if test:
    os.system('python projection_method.py --Nxc 20 --Re 10')
    exit()

#non dimensional position and velocities from Ghia et al
y = np.array([1.0, 0.9766, 0.9688, 0.9609, 0.9531, 0.8516, 0.7344, 0.6172, \
          0.5, 0.4531, 0.2813, 0.1719, 0.1016, \
          0.0703, 0.0625, 0.0547, 0.0])

x = np.array([1.0, 0.9688, 0.9609, 0.9531, 0.9453, 0.9063, 0.8594, 0.8047, \
     0.5, 0.2344, 0.2266, 0.1563, 0.0938, 0.0781, 0.0703, 0.0625, 0.0])

u_100 = np.array([1.0, 0.84123, 0.788871, 0.73722, 0.68717, 0.23151, \
         0.00332, -0.13641, -0.20581, -0.21090, -0.15662, \
        -0.10150, -0.06434, -0.04775, -0.04192, -0.03717, 0.0])

v_100 = np.array([0.0, -0.05906, -0.07391, -0.08864, -0.10313, -0.16914, -0.22445, -0.24533, \
         0.05454, 0.17527, 0.17507, 0.16077, 0.12317, 0.10890, 0.10091, 0.09233, 0.0])

u_400 = np.array([1.0, 0.75837, 0.68439, 0.61756, 0.55892, 0.29093, \
         0.16256, 0.02135, -0.11477, -0.17119, -0.32726, \
         -0.24299, -0.14612, -0.10338, -0.09266, -0.08186, 0.0])

v_400 = np.array([0.0, -0.1214, -0.15663, -0.19254, -0.22847, -0.23827, -0.44993, -0.38598, \
         0.05186, 0.30174, 0.30203, 0.28124, 0.22965, 0.20920, 0.19713, 0.1836, 0.0])

u_1000 = np.array([1.0, 0.65928, 0.57492, 0.51117, 0.46604, 0.33304, 0.18719, \
          0.05702, -0.0608, -0.10648, -0.27805, -0.38289, -0.29730, \
          -0.22220, -0.20196, -0.18109, 0.0])

v_1000 = np.array([0.0, -0.21388, -0.27669, -0.33714, -0.39188, -0.51550, -0.42665, -0.31966, \
          0.022526, 0.32235, 0.33075, 0.37095, 0.32627, 0.30353, 0.29012, 0.27485, 0.0])



#########################
#for reynold's number 100
#########################
u_lid = 10.
Lx = 1.
Ly = Lx
Reynolds_numbers = [100.0, 400.0, 1000.0]
Nxc = [40, 40, 64]
u_ghia = [u_100, u_400, u_1000]
v_ghia = [v_100, v_400, v_1000]

######
#loop over all Reynolds numbers
for i, Re in enumerate(Reynolds_numbers):
    #get data
    if not os.path.exists(npy_store_path + 'uvel_re_'+str(Re)+'_n_'+str(Nxc[i])+'.npy'):
        os.system('python projection_method.py --Nxc '+str(Nxc[i])+' --Re '+str(Re)+' --u_lid '+str(u_lid)+' --Lx '+str(Lx))
    #load simulation results
    U = np.load(npy_store_path + 'uvel_re_'+str(Re)+'_n_'+str(Nxc[i])+'.npy')
    y_prime = np.load(npy_store_path + 'y_re_'+str(Re)+'_n_'+str(Nxc[i])+'.npy')
    V = np.load(npy_store_path + 'vvel_re_'+str(Re)+'_n_'+str(Nxc[i])+'.npy')
    x_prime = np.load(npy_store_path + 'x_re_'+str(Re)+'_n_'+str(Nxc[i])+'.npy')
    P = np.load(npy_store_path + 'p_re_'+str(Re)+'_n_'+str(Nxc[i])+'.npy')
    Psi = np.load(npy_store_path + 'psi_re_'+str(Re)+'_n_'+str(Nxc[i])+'.npy')

    #Streamlines 
    xx = np.linspace(0, Lx, Nxc[i])
    yy = np.linspace(0, Lx, Nxc[i])
    X, Y = np.meshgrid(xx, yy)
    U_prime = U[1 : Nxc[i] + 1, 1 : Nxc[i] + 1]
    V_prime = V[1 : Nxc[i] + 1, 1 : Nxc[i] + 1]
    plt.streamplot(X, Y, U_prime, V_prime, density = [2, 2])
    plt.xlim((-0.2, Lx + 0.2))
    plt.ylim((-0.2, Lx + 0.2))
    plt.title(figure_folder + 'Streamlines: $Re=$ '+str(Re)+'; $N=$ '+str(Nxc[i]))
    plt.savefig(figure_folder + 'streamplot_re_'+str(int(Re))+'_n_'+str(Nxc[i])+'.pdf')
    plt.close()

    #plot contours of U, V, P
    plt.contour(U[1:Nxc[i] + 2, 1:Nxc[i] + 1])
    plt.contourf(U[1:Nxc[i] + 2, 1:Nxc[i] + 1])
    plt.title('$U$ velocity contour at $Re=$ '+str(Re)+'; $N=$ '+str(Nxc[i]))
    plt.savefig(figure_folder + 'u_contour_re_'+str(int(Re))+'_n_'+str(Nxc[i])+'.pdf')
    plt.close()
    
    plt.contour(V[1:Nxc[i] + 1, 1:Nxc[i] + 2])
    plt.contourf(V[1:Nxc[i] + 1, 1:Nxc[i] + 2])
    plt.title('$V$ velocity contour at $Re=$ '+str(Re)+'; $N=$ '+str(Nxc[i]))
    plt.savefig(figure_folder + 'v_contour_re_'+str(int(Re))+'_n_'+str(Nxc[i])+'.pdf')
    plt.close()

    plt.contour(P[1:Nxc[i] + 1, 1:Nxc[i] + 1])
    plt.contourf(P[1:Nxc[i] + 1, 1:Nxc[i] + 1])
    plt.title('Pressure contour at $Re=$ '+str(Re)+'; $N=$ '+str(Nxc[i]))
    plt.savefig(figure_folder + 'p_contour_re_'+str(int(Re))+'_n_'+str(Nxc[i])+'.pdf')
    plt.close()

    #plot U variation
    U_prime = U[1:Nxc[i] + 1, Nxc[i]/2 + 1] / u_lid
    plt.plot(U_prime, y_prime, linewidth = 2.2, label = '$Re=$'+str(Re)+'; $N=$'+str(Nxc[i]))
    plt.plot(u_ghia[i], y, 'r^', markersize = 6., label = 'Ghia et al; $Re=$'+str(Re))
    plt.legend(loc = 'best')
    plt.xlabel('Non dimensional velocity along vertical centerline, $u/u_{lid}$')
    plt.ylabel('Non dimensional position, $y/L$')
    plt.title('$U$ velocity along the centerline for Reynolds number $Re=$'+str(Re)+'; $N=$'+str(Nxc[i]))
    plt.savefig(figure_folder + 'uvel_vs_position_re_'+str(int(Re))+'_n_'+str(Nxc[i])+'.pdf')
    plt.close()

    #Plot V variation
    V_prime = V[Nxc[i]/2 + 1, 1:Nxc[i] + 1] / u_lid
    plt.plot(x_prime, V_prime, linewidth = 2.2, label = '$Re=$'+str(Re)+'; $N=$'+str(Nxc[i]))
    plt.plot(x, v_ghia[i], 'r^', markersize = 6., label = 'Ghia et al; $Re=$'+str(Re))
    plt.legend(loc = 'best')
    plt.ylabel('Non dimensional velocity along horizontal centerline, $v/u_{lid}$')
    plt.xlabel('Non dimensional position, $x/L$')
    plt.title('$V$ velocity along the centerline for Reynolds number $Re=$'+str(Re)+'; $N=$'+str(Nxc[i]))
    plt.savefig(figure_folder + 'vvel_vs_position_re_'+str(int(Re))+'_n_'+str(Nxc[i])+'.pdf')
    plt.close()
    
    #plot Psi contour
    plt.contour(Psi)
    plt.contourf(Psi)
    plt.xlabel('$x$')
    plt.ylabel('$y$')
    plt.title('Stream function $\psi$ contours for Reynolds number $Re=$'+str(Re)+'; $N=$'+str(Nxc[i]))
    plt.savefig(figure_folder + 'psi_contour_re_'+str(int(Re))+'_n_'+str(Nxc[i])+'.pdf')
    plt.close()
    
    
    print 'Re = '+str(Re)+' DONE !'

#####################################################################################
#Now make the comparison plot for Re=1000 and Nxc = 128, 64, 32 and compare with Ghia
#####################################################################################
Nxc = [32, 64, 128]
Re = 1000
#U velocity
for i, N in enumerate(Nxc):
    U = np.load(npy_store_path + 'uvel_re_1000.0_n_'+str(N)+'.npy')
    y_prime = np.load(npy_store_path + 'y_re_1000.0_n_'+str(N)+'.npy')
    U_prime = U[1:N + 1, N/2 + 1] / u_lid
    plt.plot(U_prime, y_prime, linewidth = 2.0, label = '$Re=$'+str(Re)+'; $N=$'+str(N))
plt.plot(u_1000, y, '^', markersize = 8., label = 'Ghia et al.; $Re=1000$')
plt.xlabel('Non dimensional velocity along vertical centerline, $u/u_{lid}$')
plt.ylabel('Non dimensional position, $y/L$')
plt.legend(loc = 'best')
plt.savefig(figure_folder + 'comparison_uvel_re_1000.pdf')
plt.close()

#V velocity
for i, N in enumerate(Nxc):
    V = np.load(npy_store_path + 'vvel_re_1000.0_n_'+str(N)+'.npy')
    x_prime = np.load(npy_store_path + 'x_re_1000.0_n_'+str(N)+'.npy')
    V_prime = V[N/2 + 1, 1:N + 1] / u_lid
    plt.plot(x_prime, V_prime, linewidth = 2.0, label = '$Re=$'+str(Re)+'; $N=$'+str(N))
plt.plot(x, v_1000, '^', markersize = 8., label = 'Ghia et al.; $Re=1000$')
plt.legend(loc = 'best')
plt.ylabel('Non dimensional velocity along horizontal centerline, $v/u_{lid}$')
plt.xlabel('Non dimensional position, $x/L$')
plt.savefig(figure_folder + 'comparison_vvel_re_1000.pdf')
plt.close()
    
keyboard()