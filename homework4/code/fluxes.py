#!/usr/bin/env python

import numpy as np
import sys
import os
from pdb import set_trace as keyboard

__all__ = ['convective_U', 'convective_V', 'diffusive_U', 'diffusive_V']

def convective_U(U, V, R, Nxc, dxc, dyc):
    for i in xrange(2, Nxc + 1):
        for j in xrange(1, Nxc + 1):
            ue = 0.5 * (U[i, j] + U[i + 1, j])
            uw = 0.5 * (U[i - 1, j] + U[i, j])
            duudx = (ue**2 - uw**2) / dxc
            un = 0.5 * (U[i, j] + U[i, j + 1])
            us = 0.5 * (U[i, j - 1] + U[i, j])
            vn = 0.5 * (V[i - 1, j + 1] + V[i, j + 1])
            vs = 0.5 * (V[i - 1, j] + V[i, j])
            duvdy = ((un * vn) - (us * vs)) / dyc
            R[i, j] = duudx + duvdy
    return R

def diffusive_U(U, R, Nxc, dxc, dyc):
    for i in xrange(2, Nxc + 1):
        for j in xrange(1, Nxc + 1):
            dudxe = (U[i + 1, j] - U[i, j]) / dxc
            dudxw = (U[i, j] - U[i - 1, j]) / dxc
            du2dx2 = (dudxe - dudxw) / dxc
            dudyn = (U[i, j + 1] - U[i, j]) / dyc
            dudys = (U[i, j] - U[i, j - 1]) / dyc
            du2dy2 = (dudyn - dudys) / dyc
            R[i, j] = du2dx2 + du2dy2
    return R

def convective_V(U, V, R, Nxc, dxc, dyc):
    for i in xrange(1, Nxc + 1):
        for j in xrange(2, Nxc + 1):
            ve = 0.5 * (V[i, j] + V[i + 1, j])
            vw = 0.5 * (V[i - 1, j] + V[i, j])
            ue = 0.5 * (U[i + 1, j - 1] + U[i + 1, j])
            uw = 0.5 * (U[i, j] + U[i, j - 1])
            duvdx = ((ue * ve) - (uw * vw)) / dxc
            vn = 0.5 * (V[i, j] + V[i, j + 1])
            vs = 0.5 * (V[i, j - 1] + V[i, j])
            dvvdy = ((vn ** 2) - (vs ** 2)) / dyc
            R[i, j] = duvdx + dvvdy
    return R

def diffusive_V(V, R, Nxc, dxc, dyc):
    for i in xrange(1, Nxc + 1):
        for j in xrange(2, Nxc + 1):
            dvdxe = (V[i + 1, j] - V[i, j]) / dxc
            dvdxw = (V[i, j] - V[i - 1, j]) / dxc
            dv2dx2 = (dvdxe - dvdxw) / dxc
            dvdyn = (V[i, j + 1] - V[i, j]) / dyc
            dvdys = (V[i, j] - V[i, j - 1]) / dyc
            dv2dy2 = (dvdyn - dvdys) / dyc
            R[i, j] = dv2dx2 + dv2dy2
    return R