import numpy as np
import sys
import os
from pdb import set_trace as keyboard
import scipy.sparse as scisparse
import scipy.sparse.linalg as scisparselinalg
import scipy.linalg as scilinalg
import matplotlib.pyplot as plt


__all__ = ['create_DivGrad_operator']

possible_boundary_conditions = ["Homogeneous Dirichlet","Homogeneous Neumann", "Periodic"]


def create_DivGrad_operator(Dxc,
                            Dyc,
                            Xc,
                            Yc,
                            pressurecellsmask,
                            boundary_conditions="Homogeneous Neumann"):
     
     
     """
     A routine to generate Laplacian Operator. Needed in the Pressure
     Poisson Equation.
     
     Arguments:
     
     ::Param Dxc:: type<numpy.ndarray> cell spacing in the x direction.
     ::Param Dyc:: type<numpy.ndarray> cell spacing in the y direction.
     ::Param Xc:: type<numpy.ndarray> x-coordinates of all the pressure
                                      cell centers.
     ::Param Yc:: type<numpy.ndarray> x-coordinates of all the pressure
                                      cell centers.
     ::Param pressurecellsmask:: <numpy.ndarray> Indicates which pressure
     cells need to be masked out.
     ::Param boundary_conditions:: <string> Boundary conditions.
     """
     
     #Check if supplied boundary condition is within the list of
     #acceptable boundary conditions.
     
     if not(boundary_conditions in possible_boundary_conditions):
         sys.exit("Boundary conditions need to be one of these: " +
                  repr(possible_boundary_conditions))
         

     # numbering with -1 means that it is not a fluid cell (i.e. either ghost cell or external)
     numberedpressurecells = -np.ones(Xc.shape,dtype='int64')
     jj_c,ii_c = np.where(pressurecellsmask==True)

     # total number of pressure nodes, not necessarily equal to NXc*NYc
     Np = len(jj_c)         
     numberedpressurecells[jj_c,ii_c] = range(0, Np)
     
     
     #Get 1/\Delta Xc and 1./\Delta Yc. (Pressure cell spacing)
     inv_Dxc = 1./Dxc[jj_c,ii_c]
     inv_Dyc = 1./Dyc[jj_c,ii_c]
     

     #Get 1/\Delta Xe and also Xw, Yn, Ys
     inv_DxE = 1./(Xc[jj_c,ii_c+1]-Xc[jj_c,ii_c])
     inv_DyN = 1./(Yc[jj_c+1,ii_c]-Yc[jj_c,ii_c])
     inv_DxW = 1./(Xc[jj_c,ii_c]-Xc[jj_c,ii_c-1])
     inv_DyS = 1./(Yc[jj_c,ii_c]-Yc[jj_c-1,ii_c])
     
     #define a sparse matrix to store the divgrad operator  
     DivGrad = scisparse.lil_matrix((Np,Np),dtype="float64")
     
     #numbered center, east, west, north and south cell
     iC = numberedpressurecells[jj_c,ii_c]
     iE = numberedpressurecells[jj_c,ii_c+1]
     iW = numberedpressurecells[jj_c,ii_c-1]
     iS = numberedpressurecells[jj_c-1,ii_c]
     iN = numberedpressurecells[jj_c+1,ii_c]
     
     ## if east node is inside domain
     east_node_mask = (iE!=-1)
     ii_center = iC[east_node_mask]
     ii_east   = iE[east_node_mask]
     inv_dXc_central = inv_Dxc[ii_center]
     inv_dXc_east    = inv_DxE[ii_center]
     DivGrad[ii_center, ii_east]   += inv_dXc_central * inv_dXc_east
     DivGrad[ii_center,ii_center] -= inv_dXc_central*inv_dXc_east

     
     ## if west node is inside domain
     west_node_mask = (iW!=-1)
     ii_center  = iC[west_node_mask]
     ii_west    = iW[west_node_mask]
     inv_dXc_central = inv_Dxc[ii_center]
     inv_dXc_west    = inv_DxW[ii_center]
     DivGrad[ii_center,ii_west]   += inv_dXc_central * inv_dXc_west
     DivGrad[ii_center,ii_center] -= inv_dXc_central * inv_dXc_west

     ## if north node is inside domain
     north_node_mask = (iN!=-1)
     ii_center  = iC[north_node_mask]
     ii_north   = iN[north_node_mask]
     inv_dYc_central  = inv_Dyc[ii_center]
     inv_dYc_north    = inv_DyN[ii_center]
     DivGrad[ii_center,ii_north]   += inv_dYc_central * inv_dYc_north
     DivGrad[ii_center,ii_center]  -= inv_dYc_central * inv_dYc_north

      ## if south node is inside domain
     south_node_mask = (iS!=-1)
     ii_center  = iC[south_node_mask]
     ii_south   = iS[south_node_mask]
     inv_dYc_central  = inv_Dyc[ii_center]
     inv_dYc_south    = inv_DyS[ii_center]
     DivGrad[ii_center,ii_south]   += inv_dYc_central * inv_dYc_south
     DivGrad[ii_center,ii_center]  -= inv_dYc_central * inv_dYc_south

     # if Dirichlet boundary conditions are requested, need to modify operator
     if boundary_conditions == "Homogeneous Dirichlet":

          # for every east node that is 'just' outside domain
          east_node_mask = (iE == -1) & (iC != -1)
          ii_center = iC[east_node_mask]
          inv_dXc_central = inv_Dxc[ii_center]
          inv_dXc_east    = inv_DxE[ii_center]
          DivGrad[ii_center,ii_center]  -= 2. * inv_dXc_central * inv_dXc_east

          # for every west node that is 'just' outside domain
          west_node_mask = (iW==-1)&(iC!=-1)
          ii_center = iC[west_node_mask]
          inv_dXc_central = inv_Dxc[ii_center]
          inv_dXc_west    = inv_DxW[ii_center]
          DivGrad[ii_center,ii_center]  -= 2.*inv_dXc_central*inv_dXc_west

          # for every north node that is 'just' outside domain
          north_node_mask = (iN==-1)&(iC!=-1)
          ii_center = iC[north_node_mask]
          inv_dYc_central  = inv_Dyc[ii_center]
          inv_dYc_north    = inv_DyN[ii_center]
          DivGrad[ii_center,ii_center]  -= 2.*inv_dYc_central*inv_dYc_north

          # for every south node that is 'just' outside domain
          south_node_mask = (iS==-1)&(iC!=-1)
          ii_center = iC[south_node_mask]
          inv_dYc_central  = inv_Dyc[ii_center]
          inv_dYc_south    = inv_DyS[ii_center]
          DivGrad[ii_center,ii_center]  -= 2.*inv_dYc_central*inv_dYc_south
     
     return DivGrad
