#!/usr/bin/env python

import numpy as np
import scipy.sparse as scysparse
import scipy.sparse.linalg as scysplinalg
import matplotlib.pyplot as plt
from pdb import set_trace as keyboard

__all__ = ['_create_grid']

def _create_grid(Nxc = 4, Lx = 1., vorticity = False):
    '''
    Create a square computational grid with
    velocity cells staggerred wrt to the pressure cells. 
    ''' 
    Nyc = Nxc
    
    #toal number of pressure cells
    Np   = Nxc * Nyc
    
    #length of the domain in y direction 
    Ly   = Lx
    
    #create pressure cells along each coordinate direction 
    xu_initial = np.linspace(0., Lx, Nxc + 1)
    yv_initial = np.linspace(0., Ly, Nyc + 1)
    xc = 0.5 * (xu_initial[1:] + xu_initial[:-1])
    yc = 0.5 * (yv_initial[1:] + yv_initial[:-1])
    dxc = np.diff(xc)
    dyc = np.diff(yc)
    
    #create ghost cells 
    x_xu = xu_initial[:-1]
    y_xu = xu_initial[:-1] + (dxc[1]/2)
    
    x_yv = yv_initial[:-1] + (dxc[1]/2) 
    y_yv = yv_initial[:-1]
    dx_xu = np.diff(x_xu)
    dy_xu = np.diff(y_xu)
    dx_yv = np.diff(x_yv)
    dy_yv = np.diff(y_yv)
    x_xu = np.concatenate([[x_xu[0]-dx_xu[1]],x_xu,[x_xu[-1]+dx_xu[1]]])
    y_xu = np.concatenate([[y_xu[0]-dy_xu[1]],y_xu,[y_xu[-1]+dy_xu[1]]])
    x_yv = np.concatenate([[x_yv[0]-dx_yv[1]],x_yv,[x_yv[-1]+dx_yv[1]]])
    y_yv = np.concatenate([[y_yv[0]-dy_yv[1]],y_yv,[y_yv[-1]+dy_yv[1]]])
    x_xc = np.concatenate([[xc[0]-dxc[1]],xc,[xc[-1]+dxc[1]]])
    y_yc = np.concatenate([[yc[0]-dyc[1]],yc,[yc[-1]+dyc[1]]])
    
    
    #pressure cell spacing 
    dxc = np.diff(x_xc) 
    dyc = np.diff(y_yc)
    
    #average grid size
    h = np.sqrt(dxc[0] * dyc[0])
    
    
    #Velocity and Pressure grid
    [Xu, Yu] = np.meshgrid(x_xu , y_xu)
    [Xv, Yv] = np.meshgrid(x_yv , y_yv)
    [Xc,Yc]   = np.meshgrid(x_xc,y_yc)
    [Dxc,Dyc] = np.meshgrid(dxc,dyc)
    
    if vorticity == False:
        return [Xu, Yu], [Xv, Yv], [Xc, Yc], [Dxc, Dyc]
    else:
        x_psi = x_xu
        y_psi = y_yv
        x_psi = np.insert(x_psi, x_psi.shape[0], x_psi[-1] + np.diff(x_psi)[1])
        y_psi = np.insert(y_psi, y_psi.shape[0], y_psi[-1] + np.diff(y_psi)[1])
        [Xpsi, Ypsi] = np.meshgrid(x_psi, y_psi)
        return [Xu, Yu], [Xv, Yv], [Xc, Yc], [Xpsi, Ypsi], [Dxc, Dyc]

    
    
    