import os
import sys
import numpy as np
import scipy.sparse as scysparse
from pdb import set_trace as keyboard
from time import sleep
import spatial_operators
import scipy.sparse as scysparse
import scipy.sparse.linalg as spysparselinalg 
import scipy.linalg as scylinalg   
import pylab as plt
from matplotlib import rc as matplotlibrc
from datetime import datetime
from time_integration import *
from create_grid import _create_grid
from boundary_conditions import *
from fluxes import *

#Argument parser 
import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--Lx', dest = 'Lx', type = float, default = 1., help = 'Length of the domain in x direction')
parser.add_argument('--Re', dest = 'Re', type = float, default = 1000., help = 'Reynolds Number')
parser.add_argument('--Nxc', dest = 'Nxc', type = int, default = 32, help = 'Number of pressure cells along x direction')
parser.add_argument('--u_lid', dest = 'u_lid', type = float, default = 10., help = 'Lid Velocity')
args = parser.parse_args()

#swtich for plotting
plots = True

#store npy files
npy_store_path = './data/'

#define machine epsilon
machine_epsilon = np.finfo(float).eps

#define folder to save plots 
figure_folder = "../report/figures/"

#create folder 
if not os.path.exists(figure_folder):
    os.makedirs(figure_folder)

#Grid size 
Nxc = args.Nxc
Nyc = Nxc

#length of domain
Lx   = args.Lx
Ly = Lx

#define Reynold's number
Re = args.Re

#define velocity of lid
u_lid = args.u_lid

#define the kinematic viscosity
nu = (np.abs(u_lid) * Lx) / Re

#Velocity and Pressure grid
[Xu, Yu], [Xv, Yv], [Xc,Yc], [Dxc,Dyc] = _create_grid(Nxc = Nxc,
                                                      Lx = Lx)

#Cell size 
dxc = Dxc[1][1]
dyc = Dyc[1][1]

#matrix to store vorticity in the interior and stream function
omega = np.zeros((Nxc - 1, Nxc - 1))
Psi = np.zeros((Nxc + 1, Nxc + 1))

#pressure cell mask
pressureCells_Mask = np.zeros(Xc.shape)
pressureCells_Mask[1:-1,1:-1] = True

#number pressure cells 
numbered_pressureCells = -np.ones(Xc.shape,dtype='int64')
j_C,i_C = np.where(pressureCells_Mask==True)
Np = len(j_C)
numbered_pressureCells[j_C,i_C] = range(0,Np)

u_Cells_Mask = np.zeros(Xu.shape)
u_Cells_Mask [1:-1,1:-1] = True
numbered_uCells = -np.ones(Xu.shape,dtype='int64')
j_U, i_U = np.where(u_Cells_Mask==True)
Nu = len(j_U)
numbered_uCells[j_U, i_U] = range(0,Nu)

#define U grid and boundary conditions on U velocity 
U = np.zeros(Xu.shape , dtype="float64")
U = boundary_U(U, Nxc, u_lid)

v_Cells_Mask = np.zeros(Xv.shape)
v_Cells_Mask [1:-1,1:-1] = True
numbered_vCells = -np.ones(Xv.shape,dtype='int64')
j_V, i_V = np.where(v_Cells_Mask==True)
Nv = len(j_V)
numbered_vCells[j_V, i_V] = range(0,Nv)

#define V grid and initial velocity on V velocity
V = np.zeros(Xu.shape , dtype="float64")
V = boundary_V(V, Nxc)

P = np.zeros(Xc.shape)

########################################################################
#define matrices for terms in convection operator and diffusion operator
########################################################################

#Convection operators
Rc_x = np.zeros(Xu.shape , dtype="float64")
Rc_y = np.zeros(Xv.shape , dtype="float64")

#Diffusion operators 
Rd_x = np.zeros(Xu.shape , dtype="float64")
Rd_y = np.zeros(Xv.shape , dtype="float64")

#to compute divergence of the intermediate velocity field 
delu = np.zeros(Xu.shape , dtype="float64")
delv = np.zeros(Xv.shape , dtype="float64")

#Total of the convection and diffusion operators 
Rx = np.zeros(Xu.shape , dtype="float64")
Ry = np.zeros(Xv.shape , dtype="float64")
Rux = np.zeros(Xu.shape , dtype="float64")
Rvy = np.zeros(Xv.shape , dtype="float64")

#Store Pressure
P= np.zeros(Xc.shape , dtype="float64")

#tf counts the time, T is the max. time, dt is the time step and n
#is the number of iterations
tf = 0.
T = 2.5
dt = 0.001
n = 0
tol = 1e-8

#create a DivGrad operator
DivGrad = spatial_operators.create_DivGrad_operator(Dxc,
                                                    Dyc,
                                                    Xc,
                                                    Yc,
                                                    pressureCells_Mask,
                                                    boundary_conditions="Homogeneous Neumann")

DivGrad[0, :] = 0
DivGrad[0, 0] = 1

#Store the LU Decomposition of the Div Grad operator matrix to
#speed up solving the Pressure Poisson equation 
lu = spysparselinalg.splu(DivGrad.tocsc())


#Get solution at time T
while tf < T:
    #######################
    # calculation of U* , V*
    #######################
    
    #Calculate the convective terms
    Rc_x = convective_U(U, V, Rc_x, Nxc, dxc, dyc)
    Rc_y = convective_V(U, V, Rc_y, Nxc, dxc, dyc)
    
    #Calculate the diffusive terms
    Rd_x = diffusive_U(U, Rd_x, Nxc, dxc, dyc)
    Rd_y = diffusive_V(V, Rd_y, Nxc, dxc, dyc)
    
    #Net convective and diffusive terms
    Rx = -Rc_x + nu * Rd_x
    Ry = -Rc_y + nu * Rd_y

    #Explicit time advancement to compute U* and V*
    U[2:Nxc + 1, 1:Nxc + 1] += dt * Rx[2:Nxc + 1, 1:Nxc + 1]
    V[1:Nxc + 1, 2:Nxc + 1] += dt * Ry[1:Nxc + 1, 2:Nxc + 1]

    #Apply Boundary conditions
    U = boundary_U(U, Nxc, u_lid)
    V = boundary_V(V, Nxc)
    
    # divergence of velocity
    for i in xrange(1,Nxc+1):
        for j in xrange(1,Nxc+1):
            delu[i, j] = (U[i + 1, j] - U[i, j]) / dxc
            delv[i, j] = (V[i, j + 1] - V[i, j]) / dyc
    divV = (delu + delv) / dt
    divV_m = divV[j_C, i_C]
    divV_m[0] = 0
    
    # pressure poisson solver    
    P_prime = lu.solve(divV_m)
    
    #pressure correction 
    P[j_C,i_C] = P_prime
    
    #enforce homogeneous Neumann BC on pressure
    P = boundary_P(P, Nxc)
    
    #####################
    # correcting velocity
    #####################
    
    #Updating U
    for i in xrange(2, Nxc + 1):
        for j in xrange(1, Nxc + 1):
            U[i, j] = U[i, j] - dt * ((P[i, j] - P[i - 1, j]) / dxc)
            
    #Updating V
    for i in xrange(1, Nxc + 1):
        for j in xrange(2, Nxc + 1):
            V[i, j] = V[i, j] - dt * ((P[i, j] - P[i, j - 1]) / dyc)
    
    #Enforce Boundary conditions
    U = boundary_U(U, Nxc, u_lid)
    V = boundary_V(V, Nxc)
    
    print '{}   {}   {}   {}'.format(tf, n, np.max(np.abs(U)), np.max(np.abs(V)))
    
    #Update time
    tf += dt
    n += 1

#Compute vorticity inside the domain
for i in xrange(2, Nxc + 1):
    for j in xrange(2, Nxc + 1):
        omega[i - 2, j - 2] = (V[i, j] - V[i - 1, j]) / dxc + (U[i, j] - U[i, j - 1]) / dyc


#Now compute stream function
Psi = np.zeros((Nxc + 1, Nxc + 1))
mask_matrix = -np.ones(Psi.shape)
mask_matrix[1: -1, 1: -1] = True
j_c, i_c = np.where(mask_matrix == True)
N = len(j_c)
laplacian = scysparse.lil_matrix((N, N))
numbered_psi = -np.ones(Psi.shape)
numbered_psi[j_c, i_c] = np.arange(N)
q = omega.flatten()
for k in xrange(N):
    #get current location coordinates 
    i = np.where(numbered_psi == k)[0][0]
    j = np.where(numbered_psi == k)[1][0]
    
    east = numbered_psi[i, j + 1]
    west = numbered_psi[i, j - 1]
    north = numbered_psi[i + 1, j]
    south = numbered_psi[i - 1, j]
    
    #Fill weights
    laplacian[k, k] = -(2. / dxc ** 2) -(2 / dyc ** 2)
    
    if west != -1:
        laplacian[k, west] = 1. / dxc
    
    if east != -1:
        laplacian[k, east] = 1. / dxc
    
    if north != -1:
        laplacian[k, north] = 1. / dyc
    
    if south != -1:
        laplacian[k, south] = 1. / dyc

lu = spysparselinalg.splu(-laplacian.tocsc())
psi_interior = lu.solve(q)
Psi[1:-1, 1:-1] = psi_interior.reshape(omega.shape)

#Transpose everything to get correct orientation
U = np.transpose(U)
V = np.transpose(V)
P = np.transpose(P)
Psi = np.transpose(Psi)
x = Xv[Nxc/2 + 1, 1:Nxc + 1] / Lx
y = Yu[1:Nxc + 1, Nxc/2 + 1] / Lx

#Save Data
np.save(npy_store_path + 'uvel_re_'+str(Re)+'_n_'+str(Nxc)+'.npy', U)
np.save(npy_store_path + 'vvel_re_'+str(Re)+'_n_'+str(Nxc)+'.npy', V)
np.save(npy_store_path + 'p_re_'+str(Re)+'_n_'+str(Nxc)+'.npy', P)
np.save(npy_store_path + 'psi_re_'+str(Re)+'_n_'+str(Nxc)+'.npy', Psi)
np.save(npy_store_path + 'y_re_'+str(Re)+'_n_'+str(Nxc)+'.npy', y)
np.save(npy_store_path + 'x_re_'+str(Re)+'_n_'+str(Nxc)+'.npy', x)