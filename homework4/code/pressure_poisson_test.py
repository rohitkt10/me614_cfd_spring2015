import os
import sys
import numpy as np
import scipy.sparse as scysparse
from pdb import set_trace as keyboard
from time import sleep
from spatial_operators import *
import scipy.sparse as scysparse
import scipy.sparse.linalg as spysparselinalg  # sparse linear algebra
import scipy.linalg as scylinalg               # non-sparse linear algebra
import pylab as plt
from matplotlib import rc as matplotlibrc
import time # has the equivalent of tic/toc

machine_epsilon = np.finfo(float).eps

"""
Keeping track of variables

::Nxc:: Number of pressure cells along x direction
::Nyc:: Number of pressure cells along y direction
::Lx:: Length of the domain along x direction
::Ly:: Length of the domain along y direction
::xu:: x coordinates of pressure cell vertices 
::yv:: y coordinates of pressure cell vertices
::dxc:: pressure cell spacing along x direction
::dyc:: pressure cell spacing along y direction
::xc:: x coordinates of the pressure cell centers
::yc:: y coordinates of the pressure cell centers
::Xc & Yc:: cell center mesh
::Dxc & Dyc:: cell spacing thourghout the grid 

"""

pi = np.pi
# number of (pressure) cells = mass conservation cells
Nxc  = 128
Nyc  = 128
Np   = Nxc * Nyc
Lx   = 1.
Ly   = 1.

# define grid for u and v velocity components first
# and then define pressure cells locations
t_u = np.linspace(0., 1.0, Nxc + 1)

#coordinates of vertices of the non uniform pressure grid 
xu =  t_u * t_u * Lx 
yv = t_u * t_u * Ly 

# creating ghost cells for x direction 
dxu0 = np.diff(xu)[0]
dxuL = np.diff(xu)[-1]
xu = np.concatenate([[xu[0]-dxu0],xu,[xu[-1]+dxuL]])


#create ghost cell for y direction 
dyv0 = np.diff(yv)[0]
dyvL = np.diff(yv)[-1]
yv = np.concatenate([[yv[0]-dyv0],yv,[yv[-1]+dyvL]])


#Pressure cell spacing 
dxc = np.diff(xu) 
dyc = np.diff(yv)

#cell center coordinates
xc = 0.5 * (xu[:-1]+xu[1:])  
yc = 0.5 * (yv[:-1]+yv[1:])

#Cell center mesh 
[Xc,Yc]   = np.meshgrid(xc,yc)    
[Dxc,Dyc] = np.meshgrid(dxc,dyc)   

#Mask out ghost cells 
pressureCells_Mask = np.zeros(Xc.shape)
pressureCells_Mask[1:-1,1:-1] = True


# Introducing obstacle in pressure Mask
obstacle_radius = 0.2 * Lx 
distance_from_center = np.sqrt(np.power(Xc - Lx / 2., 2.0) + np.power(Yc - Ly / 2., 2.0))

#get coordinates of those pressure cell centers that lie inside the obstacle 
j_obstacle, i_obstacle = np.where(distance_from_center<obstacle_radius)
keyboard()

#mask out the obstacle hidden cell centers 
pressureCells_Mask[j_obstacle, i_obstacle] = False

# number of actual pressure cells
Np = len(np.where(pressureCells_Mask == True)[0])

#Obtain Laplacian Operator 
DivGrad = create_DivGrad_operator(Dxc,
                                  Dyc,
                                  Xc,
                                  Yc,
                                  pressureCells_Mask,
                                  boundary_conditions="Homogeneous Dirichlet")
DivGrad = DivGrad.tocsr()
#define forcing function q = 1. Solve the Poisson Equation \nabla^{2}.\phi = q
q = np.ones(Np)
phi = spysparselinalg.spsolve(DivGrad, q) 


# pouring flattened solution back in 2D array
Phi = np.zeros((Nyc + 2, Nxc + 2)) * np.nan
Phi[np.where(pressureCells_Mask==True)] = phi
#
#
##Plotting pressure contour
#plt.contour(Xc, Yc, Phi)
#plt.contourf(Xc,Yc,Phi)
#plt.colorbar()
#plt.grid(True)
#plt.show()