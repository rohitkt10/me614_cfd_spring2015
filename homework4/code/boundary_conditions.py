#!/usr/bin/env python

import numpy as np
import sys
import os
from pdb import set_trace as keyboard

__all__ = ['boundary_U', 'boundary_V', 'boundary_P', 'boundary_omega', 'boundary_psi']
#Boundary conditions on U
def boundary_U(U, Nxc, u_lid):
    U[1, 1:Nxc + 1] = 0 #Left
    U[Nxc + 1, 1:Nxc + 1] = 0 #Right
    U[1: Nxc + 2, 0] = -U[1:Nxc + 2, 1] #Bottom
    U[1 : Nxc + 2, Nxc + 1] = (2 * u_lid) - U[1 : Nxc + 2, Nxc] #Top
    return U

def boundary_V(V, Nxc):
    V[1: Nxc + 1, 1] = 0 #Bottom
    V[1: Nxc + 1, Nxc + 1] = 0 #Top
    V[0, 1:Nxc + 2] = -V[1, 1:Nxc + 2] #Left
    V[Nxc + 1, 1:Nxc + 2] = -V[Nxc, 1:Nxc + 2] #Top
    return V

def boundary_P(P, Nxc):
    P[1: Nxc + 1, 0] = P[1:Nxc + 1, 1] #Bottom
    P[1: Nxc + 1, Nxc + 1] = P[1:Nxc + 1, Nxc] #Top
    P[0, 1: Nxc + 1] = P[1, 1:Nxc + 1] #Left
    P[Nxc + 1, 1:Nxc + 1] = P[Nxc, 1:Nxc + 1] #Right
    return P

def boundary_psi(Psi):
    #Psi = 0 on the boundaries
    Psi[1, :] = 0
    Psi[Nxc + 1, :] = 0
    Psi[:, 1] = 0
    Psi[:, Nxc + 1] = 0
    return Psi

def boundary_omega(omega, Psi, dxpsi = 0.01, dypsi = 0.01):
    omega[2:Nxc + 1, 1] = -2. * Psi[2:Nxc + 1, 2] / (dypsi**2)
    omega[2:Nxc + 1, Nxc + 1] = - 2. * Psi[2:Nxc + 1, Nxc] / (dypsi**2) \
                       - (u_lid * 2.) / dxpsi
    omega[1, 2:Nxc + 1]  = - 2 * Psi[2, 2:Nxc + 1] / (dxpsi**2)
    omega[Nxc + 1, 2 : Nxc + 1] = - 2 * Psi[Nxc, 2 : Nxc + 1] / (dxpsi**2)
    return omega