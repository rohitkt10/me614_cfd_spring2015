#!/usr/bin/env python

#!/usr/bin/env python

import os
import sys
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime
import argparse
from pdb import set_trace as keyboard

#switches
test = False

#define folder to save plots 
figure_folder = "../report/figures/vorticity_stream_function/"

#create folder 
if not os.path.exists(figure_folder):
    os.makedirs(figure_folder)

#store npy files
npy_store_path = './data/'
if not os.path.exists(npy_store_path):
    os.makedirs(npy_store_path)

if test:
    os.system('python projection_method.py --Nxc 20 --Re 10')
    exit()

#non dimensional position from Ghia et al
y = [1.0, 0.9766, 0.9688, 0.9609, 0.9531, 0.8516, 0.7344, 0.6172, \
          0.5, 0.4531, 0.2813, 0.1719, 0.1016, \
          0.0703, 0.0625, 0.0547, 0.0]

x = [1.0, 0.9688, 0.9609, 0.9531, 0.9453, 0.9063, 0.8594, 0.8047, \
     0.5, 0.2344, 0.2266, 0.1563, 0.0938, 0.0781, 0.0703, 0.0625, 0.0]

#Total simulation time
T = 15




#########################
#for reynold's number 100
#########################

Re = 100.0
Nxc = 32

#get data
if not os.path.exists(npy_store_path + 'uvel_vsf_re_'+str(Re)+'_n_'+str(Nxc)+'.npy'):
    os.system('python vorticity_stream_function.py --Nxc '+str(Nxc)+' --Re '+str(Re)+' --T '+str(T))
    
#load simulation results
U_prime = np.load(npy_store_path + 'uvel_vsf_re_'+str(Re)+'_n_'+str(Nxc)+'.npy')
y_prime = np.load(npy_store_path + 'y_vsf_re_'+str(Re)+'_n_'+str(Nxc)+'.npy')
V_prime = np.load(npy_store_path + 'vvel_vsf_re_'+str(Re)+'_n_'+str(Nxc)+'.npy')
x_prime = np.load(npy_store_path + 'x_vsf_re_'+str(Re)+'_n_'+str(Nxc)+'.npy')
PSI = np.load(npy_store_path + 'psi_vsf_re_'+str(Re)+'_n_'+str(Nxc)+'.npy')
Omega = np.load(npy_store_path + 'omega_vsf_re_'+str(Re)+'_n_'+str(Nxc)+'.npy')



#Ghia's results
u_100 = [1.0, 0.84123, 0.788871, 0.73722, 0.68717, 0.23151, \
         0.00332, -0.13641, -0.20581, -0.21090, -0.15662, \
        -0.10150, -0.06434, -0.04775, -0.04192, -0.03717, 0.0]

v_100 = [0.0, -0.05906, -0.07391, -0.08864, -0.10313, -0.16914, -0.22445, -0.24533, \
         0.05454, 0.17527, 0.17507, 0.16077, 0.12317, 0.10890, 0.10091, 0.09233, 0.0]

keyboard()


#plot U variation
plt.plot(U_prime, y_prime, linewidth = 2.5, marker = 'o', label = '$Re=$'+str(Re)+'; $N=$'+str(Nxc))
plt.plot(u_100, y, linewidth = 2.5, marker = 'o', label = 'Ghia et al; $Re=$'+str(Re))
plt.legend(loc = 'best')
plt.xlabel('Non dimensional velocity along vertical centerline, $u/u_{lid}$')
plt.ylabel('Non dimensional position, $y/L$')
plt.title('$U$ velocity along the centerline for Reynolds number $Re=$'+str(Re)+'; $N=$'+str(Nxc))
plt.savefig(figure_folder + 'uvel_vs_position_re_'+str(Re)+'_n_'+str(Nxc)+'.pdf')
plt.close()

#Plot V variation
plt.plot(x_prime, V_prime, linewidth = 2.5, marker = 'o', label = '$Re=$'+str(Re)+'; $N=$'+str(Nxc))
plt.plot(x, v_100, linewidth = 2.5, marker = '*', label = 'Ghia et al; $Re=$'+str(Re))
plt.legend(loc = 'best')
plt.ylabel('Non dimensional velocity along horizontal centerline, $v/u_{lid}$')
plt.xlabel('Non dimensional position, $x/L$')
plt.title('$V$ velocity along the centerline for Reynolds number $Re=$'+str(Re)+'; $N=$'+str(Nxc))
plt.savefig(figure_folder + 'vvel_vs_position_re_'+str(Re)+'_n_'+str(Nxc)+'.pdf')
plt.close()

#Plot PSI contour
plt.contour(PSI)
plt.contourf(PSI)
plt.savefig(figure_folder + 'psi_contour_re_'+str(Re)+'_n_'+str(Nxc)+'.pdf')
plt.close()

#Plot Omega contour
plt.contour(Omega)
plt.contourf(Omega)
plt.savefig(figure_folder + 'Omega_contour_re_'+str(Re)+'_n_'+str(Nxc)+'.pdf')
plt.close()
print 'Re = 100 DONE !'

#########################
#For re = 400
#########################
Re = 400.0
Nxc = 50

#get data
if not os.path.exists(npy_store_path + 'uvel_vsf_re_'+str(Re)+'_n_'+str(Nxc)+'.npy'):
    os.system('python vorticity_stream_function.py --Nxc '+str(Nxc)+' --Re '+str(Re)+' --T '+str(T))
    
#load simulation results
U_prime = np.load(npy_store_path + 'uvel_vsf_re_'+str(Re)+'_n_'+str(Nxc)+'.npy')
y_prime = np.load(npy_store_path + 'y_vsf_re_'+str(Re)+'_n_'+str(Nxc)+'.npy')
V_prime = np.load(npy_store_path + 'vvel_vsf_re_'+str(Re)+'_n_'+str(Nxc)+'.npy')
x_prime = np.load(npy_store_path + 'x_vsf_re_'+str(Re)+'_n_'+str(Nxc)+'.npy')
PSI = np.load(npy_store_path + 'psi_vsf_re_'+str(Re)+'_n_'+str(Nxc)+'.npy')
Omega = np.load(npy_store_path + 'omega_vsf_re_'+str(Re)+'_n_'+str(Nxc)+'.npy')

#Ghia's results
u_400 = [1.0, 0.75837, 0.68439, 0.61756, 0.55892, 0.29093, \
         0.16256, 0.02135, -0.11477, -0.17119, -0.32726, \
         -0.24299, -0.14612, -0.10338, -0.09266, -0.08186, 0.0]

v_400 = [0.0, -0.1214, -0.15663, -0.19254, -0.22847, -0.23827, -0.44993, -0.38598, \
         0.05186, 0.30174, 0.30203, 0.28124, 0.22965, 0.20920, 0.19713, 0.1836, 0.0]

#plot U variation
plt.plot(U_prime, y_prime, linewidth = 2.5, marker = 'o', label = '$Re=$'+str(Re)+'; $N=$'+str(Nxc))
plt.plot(u_400, y, linewidth = 2.5, marker = 'o', label = 'Ghia et al; $Re=$'+str(Re))
plt.legend(loc = 'best')
plt.xlabel('Non dimensional velocity along vertical centerline, $u/u_{lid}$')
plt.ylabel('Non dimensional position, $y/L$')
plt.title('$U$ velocity along the centerline for Reynolds number $Re=$'+str(Re)+'; $N=$'+str(Nxc))
plt.savefig(figure_folder + 'uvel_vs_position_re_'+str(Re)+'_n_'+str(Nxc)+'.pdf')
plt.close()

#Plot V variation
plt.plot(x_prime, V_prime, linewidth = 2.5, marker = 'o', label = '$Re=$'+str(Re)+'; $N=$'+str(Nxc))
plt.plot(x, v_400, linewidth = 2.5, marker = '*', label = 'Ghia et al; $Re=$'+str(Re))
plt.legend(loc = 'best')
plt.ylabel('Non dimensional velocity along vertical centerline, $v/u_{lid}$')
plt.xlabel('Non dimensional position, $y/L$')
plt.title('$V$ velocity along the centerline for Reynolds number $Re=$'+str(Re)+'; $N=$'+str(Nxc))
plt.savefig(figure_folder + 'vvel_vs_position_re_'+str(Re)+'_n_'+str(Nxc)+'.pdf')
plt.close()

#Plot PSI contour
plt.contour(PSI)
plt.contourf(PSI)
plt.savefig(figure_folder + 'psi_contour_re_'+str(Re)+'_n_'+str(Nxc)+'.pdf')
plt.close()

#Plot Omega contour
plt.contour(Omega)
plt.contourf(Omega)
plt.savefig(figure_folder + 'Omega_contour_re_'+str(Re)+'_n_'+str(Nxc)+'.pdf')
plt.close()


print 'Re = 400 DONE !'
keyboard()

#########################
#for Re = 1000
#########################
Re = 1000.0
Nxc = 50


#get data
if not os.path.exists(npy_store_path + 'uvel_vsf_re_'+str(Re)+'_n_'+str(Nxc)+'.npy'):
    os.system('python vorticity_stream_function.py --Nxc '+str(Nxc)+' --Re '+str(Re)+' --T '+str(T)+' --dt '+str(0.002))
    
#load simulation results
U_prime = np.load(npy_store_path + 'uvel_vsf_re_'+str(Re)+'_n_'+str(Nxc)+'.npy')
y_prime = np.load(npy_store_path + 'y_vsf_re_'+str(Re)+'_n_'+str(Nxc)+'.npy')
V_prime = np.load(npy_store_path + 'vvel_vsf_re_'+str(Re)+'_n_'+str(Nxc)+'.npy')
x_prime = np.load(npy_store_path + 'x_vsf_re_'+str(Re)+'_n_'+str(Nxc)+'.npy')
PSI = np.load(npy_store_path + 'psi_vsf_re_'+str(Re)+'_n_'+str(Nxc)+'.npy')
Omega = np.load(npy_store_path + 'omega_vsf_re_'+str(Re)+'_n_'+str(Nxc)+'.npy')

#Ghia's results
u_1000 = [1.0, 0.65928, 0.57492, 0.51117, 0.46604, 0.33304, 0.18719, \
          0.05702, -0.0608, -0.10648, -0.27805, -0.38289, -0.29730, \
          -0.22220, -0.20196, -0.18109, 0.0]

v_1000 = [0.0, -0.21388, -0.27669, -0.33714, -0.39188, -0.51550, -0.42665, -0.31966, \
          0.022526, 0.32235, 0.33075, 0.37095, 0.32627, 0.30353, 0.29012, 0.27485, 0.0]


#plot U variation
plt.plot(U_prime, y_prime, linewidth = 2.5, marker = 'o', label = '$Re=$'+str(Re)+'; $N=$'+str(Nxc))
plt.plot(u_1000, y, linewidth = 2.5, marker = 'o', label = 'Ghia et al; $Re=$'+str(Re))
plt.legend(loc = 'best')
plt.xlabel('Non dimensional velocity along vertical centerline, $u/u_{lid}$')
plt.ylabel('Non dimensional position, $y/L$')
plt.title('$U$ velocity along the centerline for Reynolds number $Re=$'+str(Re)+'; $N=$'+str(Nxc))
plt.savefig(figure_folder + 'uvel_vs_position_re_'+str(Re)+'_n_'+str(Nxc)+'.pdf')
plt.close()

#Plot V variation
plt.plot(x_prime, V_prime, linewidth = 2.5, marker = 'o', label = '$Re=$'+str(Re)+'; $N=$'+str(Nxc))
plt.plot(x, v_400, linewidth = 2.5, marker = '*', label = 'Ghia et al; $Re=$'+str(Re))
plt.legend(loc = 'best')
plt.ylabel('Non dimensional velocity along vertical centerline, $v/u_{lid}$')
plt.xlabel('Non dimensional position, $x/L$')
plt.title('$U$ velocity along the centerline for Reynolds number $Re=$'+str(Re)+'; $N=$'+str(Nxc))
plt.savefig(figure_folder + 'vvel_vs_position_re_'+str(Re)+'_n_'+str(Nxc)+'.pdf')
plt.close()

#Plot PSI contour
plt.contour(PSI)
plt.contourf(PSI)
plt.savefig(figure_folder + 'psi_contour_re_'+str(Re)+'_n_'+str(Nxc)+'.pdf')
plt.close()

#Plot Omega contour
plt.contour(Omega)
plt.contourf(Omega)
plt.savefig(figure_folder + 'Omega_contour_re_'+str(Re)+'_n_'+str(Nxc)+'.pdf')
plt.close()

print 'Re = 1000 DONE !'






