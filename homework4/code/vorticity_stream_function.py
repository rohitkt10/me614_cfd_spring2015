#!/usr/bin/env python

import os
import sys
import numpy as np
import scipy.sparse as scysparse
from pdb import set_trace as keyboard
from time import sleep
import spatial_operators
import scipy.sparse as scysparse
import scipy.sparse.linalg as spysparselinalg 
import scipy.linalg as scylinalg   
import pylab as plt
from matplotlib import rc as matplotlibrc
from datetime import datetime
from time_integration import *
from create_grid import *
import argparse


#Parse arguments
parser = argparse.ArgumentParser()
parser.add_argument('--Lx', dest = 'Lx', type = float, default = 1., help = 'Length of the domain in x direction')
parser.add_argument('--Re', dest = 'Re', type = float, default = 400., help = 'Reynolds Number')
parser.add_argument('--Nxc', dest = 'Nxc', type = int, default = 41, help = 'Number of cells')
parser.add_argument('--u_lid', dest = 'u_lid', type = float, default = 10., help = 'Lid Velocity')
parser.add_argument('--mit', dest = 'mit', type = int, default = 10000, help = 'Maximum number of time steps')
parser.add_argument('--dt', dest = 'dt', type = float, default = 0.002, help = 'Time step')
parser.add_argument('--T', dest = 'T', type = float, default = 2.5, help = 'Total time')
args = parser.parse_args()

#Boundary conditions on Psi
def boundary_psi(Psi, dxpsi = 0.01, dypsi = 0.01, u_lid = 10):
    #Psi = 0 on the boundaries
    Psi[1, :] = 0
    Psi[Nxc + 1, :] = 0
    Psi[:, 1] = 0
    Psi[:, Nxc + 1] = 0
    return Psi

def boundary_omega(omega, Psi, dxpsi = 0.01, dypsi = 0.01):
    omega[2:Nxc + 1, 1] = -2. * Psi[2:Nxc + 1, 2] / (dypsi**2)
    omega[2:Nxc + 1, Nxc + 1] = - 2. * Psi[2:Nxc + 1, Nxc] / (dypsi**2) \
                       - (u_lid * 2.) / dxpsi
    omega[1, 2:Nxc + 1]  = - 2 * Psi[2, 2:Nxc + 1] / (dxpsi**2)
    omega[Nxc + 1, 2 : Nxc + 1] = - 2 * Psi[Nxc, 2 : Nxc + 1] / (dxpsi**2)
    return omega

#swtich for plotting
plots = True

#store npy files
npy_store_path = './data/'

#define machine epsilon
machine_epsilon = np.finfo(float).eps

#define folder to save plots 
figure_folder = "../report/figures/"

#create folder 
if not os.path.exists(figure_folder):
    os.makedirs(figure_folder)

#Grid size
#Nxc = 4
Nxc = args.Nxc
Nyc = Nxc

#length of domain
Lx   = args.Lx
Ly = Lx

#define Reynold's number
Re = args.Re

#define velocity of lid
u_lid = args.u_lid


#define the kinematic viscosity
nu = (np.abs(u_lid) * Lx) / Re

#generate grid
[Xu, Yu], [Xv, Yv], [Xc, Yc], [Xpsi, Ypsi], [Dxpsi, Dypsi] = _create_grid(Nxc = Nxc,
                                                                          vorticity = True)

T = args.T

#keyboard()
#Initialize Psi and omega to zero
Psi = np.zeros(Xpsi.shape)
omega = np.zeros(Xpsi.shape)
omega_old = np.zeros(omega.shape)
R = np.zeros(omega.shape)
U = np.zeros(Xu.shape)
V = np.zeros(Xv.shape)
dxpsi = Dxpsi[1][1]
dypsi = Dypsi[1][1]
#keyboard()
#Enforce boundary conditions
Psi = boundary_psi(Psi,
                   dxpsi = dxpsi,
                   dypsi = dypsi,
                   u_lid = u_lid)
omega = boundary_omega(omega,
                       Psi,
                       dxpsi = dxpsi,
                       dypsi = dypsi)

#keyboard()
##create a 2nd order differential operator to solve poisson equation
#D2 = laplacian_operator(Nxc = Nxc,
#                             dx = dxpsi,
#                             dy = dypsi)

#time step:
dt = args.dt

#Maximum number of iterations
mit = args.mit

#Count the number of iterations
n = 0

#Count time
tf = 0

def sor(Psi, Nxc, dxpsi, dypsi, omega,
        alpha = 1.5, max_it = 100, tol = 0.001):
    error = 0.
    for n in xrange(max_it):
        for i in xrange(2, Nxc + 1):
            for j in xrange(2, Nxc + 1):
                Psi[i, j] = 0.25 * alpha * (Psi[i + 1, j] + \
                                            Psi[i - 1, j] + \
                                            Psi[i, j + 1] + \
                                            Psi[i, j - 1] + \
                                            dxpsi * dypsi * omega[i, j]) \
                                            + (1 - alpha) * Psi[i, j]
        
        Psi = boundary_psi(Psi, dxpsi = dxpsi,
                           dypsi = dypsi, u_lid = u_lid)
        
        #error criteria
        psi = Psi[1:Nxc + 2, 1 : Nxc + 2]
        r = R[1 : Nxc + 2, 1 : Nxc + 2]
        error += np.sum(np.abs(r - psi))
        if error <= tol:
            return Psi
    return Psi
    

#start time integration
while n < mit:
    #store the value of Psi at beginning of the current time increment cycle
    Psi_old = Psi
    omega_old = omega
    
    #use SOR to compute Psi at the next time step
    Psi = sor(Psi_old, Nxc, dxpsi, dypsi, omega)
    Psi = boundary_psi(Psi, dxpsi = dxpsi,
                       dypsi = dypsi, u_lid = u_lid)
    
    #Solve Vorticity Equation to update omega
    for i in xrange(2, Nxc + 1):
        for j in xrange(2, Nxc + 1):
            #keyboard()
            #Convection term
            dpsidy = (Psi[i, j + 1] - Psi[i, j - 1]) / (2 * dypsi)
            dpsidx = (Psi[i + 1, j] - Psi[i - 1, j]) / (2 * dxpsi)
            dwdx = (omega[i + 1, j] - omega[i - 1, j]) / (2 * dxpsi)
            dwdy = (omega[i, j - 1] - omega[i, j + 1]) / (2 * dypsi)
            Rc = -(dpsidy * dwdx) + (dpsidx * dwdy)
            
            #diffusion term
            d2dx2w = (omega[i + 1, j] + omega[i - 1, j] - 2*omega[i, j]) / (dypsi ** 2)
            d2dy2w = (omega[i, j + 1] + omega[i, j - 1] - 2*omega[i, j]) / (dxpsi ** 2)
            Rd = nu * (d2dx2w + d2dy2w)
        
            #Net convection diffusion term
            R[i, j] = Rc + Rd
    

    #Time advancement using explicit Euler
    omega[2:Nxc + 1, 2:Nxc + 1] += dt * R[2:Nxc + 1, 2:Nxc + 1]
    #omega = time_advance_explicit(omega, Rx = R, Nxc = Nxc + 1,
    #                              time_advancement = 'RK1',
    #                              dt = 0.0002, tol = 0.00001)
    omega = boundary_omega(omega,
                           Psi,
                           dxpsi = dxpsi,
                           dypsi = dypsi)
    
    PSI = np.transpose(Psi[1: Nxc + 2, 1: Nxc + 2])
    Omega = np.transpose(omega[1: Nxc + 2, 1: Nxc + 2])
    
    #U Velocity = dpsi/dy
    for i in xrange(1, Nxc + 1):
        for j in xrange(1, Nxc + 2):
            U[i, j] = (PSI[i, j - 1] - PSI[i - 1, j - 1]) / dypsi
    U[0, :] = -U[1, :]
    U[Nxc + 1, :] = (2 * u_lid) - U[:, Nxc]
    
    #V Velocity = -dpsi/dx
    for i in xrange(1, Nxc + 2):
        for j in xrange(1, Nxc + 1):
            V[i, j] = -(PSI[i - 1, j] - PSI[i - 1, j - 1]) / dxpsi
    V[:, 0] = -V[:, 1]
    V[:, Nxc + 1] = -V[:, Nxc]
    
    if tf >= T:
        break
    n += 1
    tf += dt
    print '{}   {}   {}   {}'.format(n, tf, np.min(Psi), np.min(omega))

keyboard()
#save data    
np.save(npy_store_path + 'psi_vsf_re_'+str(Re)+'_n_'+str(Nxc)+'.npy', PSI)
np.save(npy_store_path + 'omega_vsf_re_'+str(Re)+'_n_'+str(Nxc)+'.npy', Omega)
U_prime = (U[1:Nxc + 1, Nxc/2 + 1]) / u_lid
y = (Yu[1:Nxc + 1, Nxc/2 + 1]) / Ly
np.save(npy_store_path + 'uvel_vsf_re_'+str(Re)+'_n_'+str(Nxc)+'.npy', U_prime)
np.save(npy_store_path + 'y_vsf_re_'+str(Re)+'_n_'+str(Nxc)+'.npy', y)
V_prime = (V[Nxc/2 + 1, 1 : Nxc + 1]) / u_lid
x = (Xv[Nxc/2 + 1, 1 : Nxc + 1]) / Lx
np.save(npy_store_path + 'vvel_vsf_re_'+str(Re)+'_n_'+str(Nxc)+'.npy', V_prime)
np.save(npy_store_path + 'x_vsf_re_'+str(Re)+'_n_'+str(Nxc)+'.npy', x)


keyboard()

    
    




