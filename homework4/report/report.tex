%%% Preamble
\documentclass[paper=a4, fontsize=12pt, titlepage]{article}
%\usepackage{fourier}
%\usepackage{subfigure}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{setspace} 
%\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{listings}
%\usepackage[margin=1cm]{geometry}
\usepackage{float}
\usepackage{subcaption}

\usepackage[english]{babel}															% English language/hyphenation
\usepackage[protrusion=true,expansion=true]{microtype}	
%\usepackage{amsmath,amsfonts,amsthm} % Math packages
\usepackage[pdftex]{graphicx}	
\graphicspath{ {/version_control_repos/bitbucket_repos/me614_cfd/me614_cfd_spring2015/homework4/report/figures/} }

\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhead{}
\fancyfoot{}
\fancyhead[LO, LE]{\thepage}

%%% Equation and float numbering
\numberwithin{equation}{section}		% Equationnumbering: section.eq#
\numberwithin{figure}{section}			% Figurenumbering: section.fig#
\numberwithin{table}{section}				% Tablenumbering: section.tab#

\makeatletter							% Author
\def\printauthor{%					
    {\centering \large \@author}}				
\makeatother		


%%% Maketitle metadata
\newcommand{\horrule}[1]{\rule{\linewidth}{#1}} 	% Horizontal rule

\title{
		\normalfont \Large \textsc{\textbf{ME614 - Computational Fluid Dynamics}} \\ [30pt]
		\horrule{0.5pt} \\[0.4cm]
		\huge 2-D Incompressible Navier Stokes Solver for the Lid Driven Cavity Problem \\
		\horrule{2pt} \\[0.5cm]
}
\author{\Large \textbf{Rohit Tripathy} \\ School of Mechanical Engineering \\ Purdue University \\ \href{mailto:rtripath@purdue.edu}{rtripath@purdue.edu}}

\date{}
\begin{document}
\maketitle

\section{Introduction}
%General lid driven cavity problem
The Lid Driven Cavity problem is a standard test case problem in Fluid Mechanics simulations to test new algorithms or solution methods. The general setup of the problem is show below.

\begin{figure}[H]
\includegraphics[scale = 1.0]{general_ldc_setup}
\caption{General setup of the Lid Driven Cavity Problem}
\end{figure}

This popularity of the Lid Driven cavity problem as a test case is due to the following reasons:
\begin{enumerate}
\item \textbf{Geometry} - The domain is 2 dimensional and is usually taken to be a square
\item\textbf{Boundary Conditions} - Dirichlet boundary conditions on both $u$ and $v$ velocities apply on all the walls
\item \textbf{Mesh} - Even relatively coarse uniform grids have been shown to exhibit good results
\item \textbf{Existing Literature} - Owing to the simplicity of the problem, this problem has been thoroughly studied and an enormous stockpile of literature for this problem is available. This makes validating results simpler 
\end{enumerate}

%gov. equations 
\subsection{Governing Equations}
The Navier-Stokes Equations are elliptic-parabolic equations and closed form solutions are not available, bar a few special cases. Hence numerical simulation of fluids  assumes special importance in understanding the physics of fluids. For the Lid Driven Cavity problem,we need to solve the 2-D incompressible Navier Stokes Equations  which in cartesian coordinates are expressed as follows:
\begin{equation}
\frac{\partial u}{\partial t} + u\frac{\partial u}{\partial x} + v \frac{\partial u}{\partial y} = -\frac{\partial p}{\partial x} + \nu \bigg( \frac{\partial^2 u}{\partial x^2} + \frac{\partial^2 u}{\partial y^2} \bigg) 
\label{eq:ns1}
\end{equation}
\begin{equation}
\frac{\partial v}{\partial t} + u\frac{\partial v}{\partial x} + v \frac{\partial v}{\partial y} = -\frac{\partial p}{\partial y} + \nu \bigg( \frac{\partial^2 v}{\partial x^2} + \frac{\partial^2 v}{\partial y^2} \bigg) 
\label{eq:ns2}
\end{equation}
\begin{equation}
\frac{\partial u}{\partial x} + \frac{\partial v}{\partial y} = 0
\label{eq:ns_cont}
\end{equation}


\subsection{Boundary Conditions}
The Lid Driven Cavity Problem has the following Boundary conditions on Pressure and Velocity:
\begin{gather}
\frac{\partial p}{\partial n} = 0 \ on \ \partial \Omega \\
v = 0 \ on \  \partial \Omega \\
u = 
\begin{cases} 
u_{lid}, \ on \ y=L_y, \\
0, \text{otherwise}
\end{cases}
\end{gather}
\label{sec:gov_eqn}
\section{Solution Methodology}

The Navier Stokes equations as shown in the previous section, govern the flow of viscous Newtonian fluids. These are a system of coupled elliptic-parabolic equations. We choose to develop a fully conservative Finite Difference solver with a staggered grid arrangement of the primitive variables. 

\subsection{Grid generation}
A staggered grid approach means that the grid is discretized into a number of cells and the scalar transport variables are stored at the cell center locations whereas the velocity and vorticity are stored are stored at staggered locations. 
This approach is favored to a collocated approach in which all the primitive variables are stored at the same locations i.e. nodes of the discretized domain. The collocated approach results in wild oscillations in the solution leading to checkerboard patterns. This is a result of odd-even decoupling of the pressure-velocity. This problem is avoided in the staggered grid approach which ensures strong coupling of the pressure and velocity. The only disadvantage of the staggered grid approach is that it requires a fair amount of book keeping in terms of indexing of the variable locations on the grid. This is a small price to pay for the resulting improvement in solution accuracy and stability. The figure below shows a visual arrangement of variable locations in the staggered grid.

\begin{figure}[H]
\label{staggered}
\centering
\includegraphics[scale = 0.5]{staggeredgrid}
\caption{Staggered Grid Arrangement of primitive variables}
\end{figure}

\subsection{Spatial Discretization}

We adopt a fully conservative $2^{nd}$ order discretization in space. The various spatial derivative terms in the Navier-Stokes are discretized according to the following rules:

\subsubsection{Convective Terms}
\begin{align}
u\frac{ \partial u}{\partial x} = \frac{\partial (uu)}{\partial x} &= \frac{u_e^{2} - u_w^{2}}{\Delta x} \\
v\frac{ \partial u}{\partial y} = \frac{\partial (uv)}{\partial x} &= \frac{u_e v_e - u_w v_w}{\Delta y} \\
u\frac{ \partial v}{\partial x} = \frac{\partial (uv)}{\partial x} &= \frac{u_e v_e - u_w v_w}{\Delta x} \\
v\frac{ \partial v}{\partial y} = \frac{\partial (vv)}{\partial x} &= \frac{v_n^{2} - v_s^{2}}{\Delta y} 
\end{align}

\subsubsection{Diffusive Terms}
\begin{align}
\frac{\partial^2 u}{\partial x^2} = \frac{(\frac{\partial u}{\partial x})_e  - (\frac{\partial u}{\partial x})_w}{\Delta x} \\
\frac{\partial^2 u}{\partial y^2} = \frac{(\frac{\partial u}{\partial y})_n  - (\frac{\partial u}{\partial y})_s}{\Delta y} \\
\frac{\partial^2 v}{\partial x^2} = \frac{(\frac{\partial v}{\partial x})_e  - (\frac{\partial v}{\partial x})_w}{\Delta x} \\
\frac{\partial^2 v}{\partial y^2} = \frac{(\frac{\partial v}{\partial y})_n  - (\frac{\partial v}{\partial y})_s}{\Delta y}
\end{align}


\subsubsection{Pressure}
The pressure field is obtained directly by solving the Pressure-Poisson Equation and a spatial discretization strategy for the pressure is not adopted unlike the convective and diffusive fluxes.

\subsection{Temporal Discretization}
The Explicit Euler Scheme was adopted to advance the solution in time. The explicit Euler is a $1^{st}$ order accurate scheme and care needs to be taken to ensure that the CFL criterion. A better albeit more time consuming approach is adopt higher order explicit schemes like Runge Kutta ($2^{nd}$, $3^{rd}$ or $4^{th}$ order). Also note that an implicit or semi implicit scheme would be unconditionally stable and would allow the solution to march forward in time with higher step sizes. However it is computationally expensive as it requires the solution of a system of equations at each time step. The temporal derivatives are discretized as follows:

\begin{equation}
\frac{\partial u_i}{\partial t} = \frac{u^{n+1} - u^{n}}{\Delta t}
\end{equation}

\section{Results and Validation}

The Lid Driven Cavity problem was simulated  for Reynold's Numbers of 100, 400 and 1000 for grid sizes $N = 40, 40 \text{ and } 128$ respectively. The results obtained were validated with \textit{Ghia et al} \cite{Ghia1982387}, which is a benchmark paper for Lid Driven Cavity simulations. We also present a comparison of the results obtained with $Re = 1000$ at 3 different grid sizes and compare it to Ghia's solution for the same $Re$.
\subsection{$Re = 100$}

\begin{figure}[H]
\centering
\begin{subfigure}{0.45\textwidth}
  \centering
  \includegraphics[width=\linewidth]{u_contour_re_100_n_40}
  \caption{$u$ Velocity contour}
  \label{fig:sfig1}
\end{subfigure}
\begin{subfigure}{0.45\textwidth}
  \centering
  \includegraphics[width=\linewidth]{v_contour_re_100_n_40}
  \caption{$v$ velocity contour}
  \label{fig:sfig1}
\end{subfigure}
\caption{Velocity contours}
\end{figure}


\begin{figure}[H]
\centering
\begin{subfigure}{0.85\textwidth}
  \centering 
  \includegraphics[width=\linewidth]{uvel_vs_position_re_100_n_40}
  \caption{Non-dimensionalized vertical centerline $u$ velocity}
  \label{fig:sfig1}
\end{subfigure}
\begin{subfigure}{0.85\textwidth}
  \centering
  \includegraphics[width=\linewidth]{vvel_vs_position_re_100_n_40}
  \caption{Non-dimensionalized horizontal centerline $v$ velocity}
  \label{fig:sfig2}
\end{subfigure}
\caption{Validation with \textit{Ghia et al.}; $N=40$}
\end{figure}

\begin{figure}[H]
\centering
\begin{subfigure}{0.45\textwidth}
  \centering
  \includegraphics[width=\linewidth]{streamplot_re_100_n_40}
  \caption{Streamlines}
  \label{fig:sfig1}
\end{subfigure}
\begin{subfigure}{0.45\textwidth}
  \centering
  \includegraphics[width=\linewidth]{p_contour_re_100_n_40}
  \caption{Pressure contour}
  \label{fig:sfig1}
\end{subfigure}
\caption{Streamline plot and Pressure contour for $Re = 100$}
\label{fig:fig}
\end{figure}

\subsection{$Re = 400$}

\begin{figure}[H]
\centering
\begin{subfigure}{0.45\textwidth}
  \centering
  \includegraphics[width=\linewidth]{u_contour_re_400_n_40}
  \caption{$u$ Velocity contour}
  \label{fig:sfig1}
\end{subfigure}
\begin{subfigure}{0.45\textwidth}
  \centering
  \includegraphics[width=\linewidth]{v_contour_re_400_n_40}
  \caption{$v$ velocity contour}
  \label{fig:sfig1}
\end{subfigure}
\caption{Velocity contours}
\end{figure}


\begin{figure}[H]
\centering
\begin{subfigure}{0.45\textwidth}
  \centering
  \includegraphics[width=\linewidth]{streamplot_re_400_n_40}
  \caption{Streamlines}
  \label{fig:sfig1}
\end{subfigure}
\begin{subfigure}{0.45\textwidth}
  \centering
  \includegraphics[width=\linewidth]{p_contour_re_400_n_40}
  \caption{Pressure contour}
  \label{fig:sfig1}
\end{subfigure}
\caption{Streamline plot and Pressure contour for $Re = 400$}
\label{fig:fig}
\end{figure}

\begin{figure}[H]
\centering
\begin{subfigure}{0.85\textwidth}
  \centering 
  \includegraphics[width=\linewidth]{uvel_vs_position_re_400_n_40}
  \caption{Non-dimensionalized vertical centerline $u$ velocity}
  \label{fig:sfig1}
\end{subfigure}
\begin{subfigure}{0.85\textwidth}
  \centering
  \includegraphics[width=\linewidth]{vvel_vs_position_re_400_n_40}
  \caption{Non-dimensionalized horizontal centerline $v$ velocity}
  \label{fig:sfig2}
\end{subfigure}
\caption{Validation with \textit{Ghia et al.}; $N=40$}
\end{figure}


\subsection{$Re = 1000$}


\begin{figure}[H]
\centering
\begin{subfigure}{0.85\textwidth}
  \centering 
  \includegraphics[width=\linewidth]{uvel_vs_position_re_1000_n_128}
  \caption{Non-dimensionalized vertical centerline $u$ velocity}
  \label{fig:sfig1}
\end{subfigure}
\begin{subfigure}{0.85\textwidth}
  \centering
  \includegraphics[width=\linewidth]{vvel_vs_position_re_1000_n_128}
  \caption{Non-dimensionalized horizontal centerline $v$ velocity}
  \label{fig:sfig2}
\end{subfigure}
\caption{Validation with \textit{Ghia et al.}; $N=128$}
\end{figure}

\begin{figure}[H]
\centering
\begin{subfigure}{0.45\textwidth}
  \centering
  \includegraphics[width=\linewidth]{u_contour_re_1000_n_128}
  \caption{$u$ Velocity contour}
  \label{fig:sfig1}
\end{subfigure}
\begin{subfigure}{0.45\textwidth}
  \centering
  \includegraphics[width=\linewidth]{v_contour_re_1000_n_128}
  \caption{$v$ velocity contour}
  \label{fig:sfig1}
\end{subfigure}
\caption{Velocity contours}
\end{figure}


\begin{figure}[H]
\centering
\begin{subfigure}{0.45\textwidth}
  \centering
  \includegraphics[width=\linewidth]{streamplot_re_1000_n_128}
  \caption{Streamlines}
  \label{fig:sfig1}
\end{subfigure}
\begin{subfigure}{0.45\textwidth}
  \centering
  \includegraphics[width=\linewidth]{p_contour_re_1000_n_128}
  \caption{Pressure contour}
  \label{fig:sfig1}
\end{subfigure}
\caption{Streamline plot and Pressure contour for $Re = 1000$}
\label{fig:fig}
\end{figure}


\subsection{Varying grid sizes for $Re=1000$}
\begin{figure}[H]
\centering
\begin{subfigure}{0.85\textwidth}
  \centering 
  \includegraphics[width=\linewidth]{comparison_uvel_re_1000}
  \caption{Vertical centerline $u$ velocity}
  \label{fig:sfig1}
\end{subfigure}
\begin{subfigure}{0.85\textwidth}
  \centering
  \includegraphics[width=\linewidth]{comparison_vvel_re_1000}
  \caption{Horizontal centerline $v$ velocity}
  \label{fig:sfig2}
\end{subfigure}
\caption{Comparison of results for $Re=1000$ simulated with $N=32,\ 64\ 128$}
\end{figure}

The plots clearly indicate that obtained results move closer to results by \textit{Ghia et al.} as we refine the grid.

\section{[BONUS] - Computing stream function $\psi$}

In order to compute the stream function, we first compute the vorticity from the velocity field as follows: 

\begin{equation}
\omega = \frac{\partial v}{\partial x} - \frac{\partial u}{\partial y}
\end{equation}

 Once we have the vorticity, we compute the stream function according to the following Poisson Equation: 
 
 \begin{equation}
 -\nabla^2 \psi = \omega
 \end{equation}
 
 

The boundary conditions on the stream function is as follows:

\begin{equation}
\psi = 0 \ \text{on}\  \partial \Omega
\end{equation}

\begin{figure}[H]
\centering
\begin{subfigure}{0.45\textwidth}
  \centering
  \includegraphics[width=\linewidth]{psi_contour_re_100_n_40}
  \caption{$Re=100$; $N=40$}
  \label{fig:sfig1}
\end{subfigure}
\begin{subfigure}{0.45\textwidth}
  \centering
  \includegraphics[width=\linewidth]{psi_contour_re_400_n_40}
  \caption{$Re=400$; $N=40$}
  \label{fig:sfig1}
\end{subfigure}
\begin{subfigure}{0.45\textwidth}
  \centering
  \includegraphics[width=\linewidth]{psi_contour_re_1000_n_64}
  \caption{$Re=1000$; $N=64$}
  \label{fig:sfig1}
\end{subfigure}
\begin{subfigure}{0.45\textwidth}
  \centering
  \includegraphics[width=\linewidth]{psi_contour_re_1000_n_128}
  \caption{$Re=1000$; $N=128$}
  \label{fig:sfig1}
\end{subfigure}
\caption{Flooded Isocontours of $\psi$}
\label{fig:fig}
\end{figure}

\section{Discussion}
The $2^{nd}$ order accurate FD solver developed in this project shows an adequate level of accuracy when compared to \textit{Ghia et al}'s benchmark paper on Lid Driven Cavity simulation. It is shown that the scheme is consistent in the sense that the numerical solution moves to the true solution as the grid is refined. Numerical conservation is also rigorously enforced to ensure a stable solution. By far the biggest bottleneck in the solution of a fluid mechanics problem through a fractional step method is the Pressure-Poisson equation. Inversion of the Laplacian is a computationally intensive process which scales as $\mathcal{O}(N^3)$ where $N=m \times n$, $m$ and $n$ being the number of pressure nodes along each coordinate axes. In this project we solved the Poisson equation equation through an LU Decomposition of the $\nabla^2$ operator, a process that requires $N^3 + \mathcal{O}(n^2)$ computations. Iterative methods such as Successive Over Relaxation applied to Gauss-Seidel method or Jacobi Method may also be used. 

We note that at higher Reynold's number formation of pockets of recirculating regions are formed at the bottom corner. These pockets grow larger as the Reynold's number is increased. It should also be noted that the Lid Driven Cavity problem has 2 points of singularity in the domain at the intersection of the side walls and the top wall. This is because of differing boundary conditions on these walls and this problem is inherent to the Lid Driven Cavity problem and has nothing to do with the solution method. 


\bibliographystyle{plain}
\bibliography{references} 
\end{document}