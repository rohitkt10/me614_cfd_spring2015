#!/usr/bin/env python
"""
Time advancement schemes:

1. Explicit Euler
2. Crank Nicholson
3. Implicit Euler

Advection Schemes :

1. 2nd order Upwind
2. 4th order Upwind
3. 2nd order Central
4. 4th order Central

Diffusion Schemes :

Only 2nd order Central
"""
import os
import sys
import numpy as np
from pdb import set_trace as keyboard
from time import sleep
import spatial_discretization
import scipy.sparse as scysparse
import scipy.sparse.linalg as spysparselinalg  # sparse linear algebra
import scipy.linalg as scylinalg               # non-sparse linear algebra
import pylab as plt
from matplotlib import rc as matplotlibrc
from scipy.linalg import circulant

__all__ = ['advection_scheme_operator', 'diffusion_scheme_operator',
           'A_matrix', 'B_matrix', 'generate_transition_matrix']

def advection_scheme_operator(scheme = 'second order central',
                              c = 1.,
                              N = 10,
                              periodic = 'True'):
    """
    Routine that returns the spatial discretization operator for
    advection term given the required discretization scheme, the
    wave velocity, the number of grid points and whether periodic
    domain or not.
    """
    if periodic:
        column = np.zeros(N)
        if 'central' in scheme:
            if 'second' in scheme:
                #2nd order central
                weights = (1. / 2) * np.array([-1., 0., 1.])
                for i in xrange(-1, 2):
                    column[i] = weights[i + 1]
                    
            elif 'fourth' in scheme:
                #4th order central
                weights = (1. / 12.) * np.array([-1., -8., 0., 8., 1.])
                column = np.zeros(N)
                for i in xrange(-2, 3):
                    column[i] = weights[i + 2]
                    
            else:
                #6th order central
                weights = (1. / 60) * np.array([-1., -9., -45., 0., 45., -9., 1.])
                column = np.zeros(N)
                for i in xrange(-3, 4):
                    column[i] = weights[i + 3]
                
        else:
            if 'second' in scheme:
                #second order upwind scheme
                if c > 0:
                    #for positive advection speed; use backward difference weights
                    weights = (1. / 2.) * np.array([1., -4., 3.])
                    for i in xrange(-2, 1):
                        column[i] = weights[i + 2]
                        
                    
                else:
                    #for negative advection speed; use forward difference weights 
                    weights = (1. / 2) * np.array([-3., 4., -1])
                    for i in xrange(3):
                        column[i] = weights[i]
                        
            else:
                #fourth order upwind scheme
                if c > 0:
                    #positive advection speed
                    weights = (1. / 12) * np.array([3., -16., 36., -48., 25.])
                    for i in xrange(-4, 1):
                        column[i] = weights[i + 4]
                        
                else:
                    #negative advection speed
                    weights = (1. / 12.) * np.array([-25., 48., -36., 16., -3.])
                    for i in xrange(0, 5):
                        column[i] = weights[i]
        
        #generate the circulant differential operator matrix
        A = circulant(column).T
        return A
    


def A_matrix(time_scheme = 'explicit euler',
             advection_scheme = 'second order central',
             cfl_c = 0.8,
             cfl_d = 0.8,
             N = 10,
             c = 1.):
    """
    generate the generate the a matrix given a particular time advancement
    scheme and a particular advection scheme.
    
    Diffusion scheme is 2nd order central.
    """
    if 'implicit' in time_scheme:
        #Implicit Euler Time advancement
        A = np.eye(N) - cfl_d * diffusion_scheme_operator(N) \
            + cfl_c * advection_scheme_operator(scheme = advection_scheme,
                                                c = c, N = N)
        
    elif 'explicit' in time_scheme:
        #Explicit Euler Time advancement
        A = np.eye(N)
        
    else:
        #Crank Nicholson Time advancement
        A = np.eye(N) - 0.5 * (cfl_d * diffusion_scheme_operator(N) \
            - cfl_c * advection_scheme_operator(scheme = advection_scheme,
                                                c = c, N = N))
    return A


def B_matrix(time_scheme = 'explicit euler',
             advection_scheme = 'second order central',
             cfl_c = 0.8,
             cfl_d = 0.8,
             N = 10,
             c = 1.):
    """
    generate the generate the B matrix given a particular time advancement
    scheme and a particular advection scheme.
    
    Diffusion scheme is 2nd order central.
    """
    if 'implicit' in time_scheme:
        #Implicit Euler Time advancement
        B = np.eye(N)
        
    elif 'explicit' in time_scheme:
        #Explicit Euler Time advancement
        B = np.eye(N) + cfl_d * diffusion_scheme_operator(N) \
            + cfl_c * advection_scheme_operator(scheme = advection_scheme,
                                                c = c, N = N)
        
    else:
        #Crank Nicholson Time advancement
        B = np.eye(N) + 0.5 * (cfl_d * diffusion_scheme_operator(N) \
            - cfl_c * advection_scheme_operator(scheme = advection_scheme,
                                                c = c, N = N))
    return B


    

def generate_transition_matrix(A = None, B = None):
    """
    generate transition matrix given A and B matrices for
    a transient problem
    """
    assert A != None and B != None
    return np.linalg.solve(A, B)



def diffusion_scheme_operator(N = 10):
    """
    2nd order central difference operator for diffusion term.
    
    ::param N:: NUmber of grid points.
    
    Returns a NXN differential operator matrix 
    """
    weights = np.array([1., -2., 1.])
    column = np.zeros(N)
    for i in xrange(-1, 2):
        column[i] = weights[i + 1]
    A = circulant(column).T
    return A