#!/usr/bin/env python

import numpy as np
import math
import matplotlib.pyplot as plt
import os

#Ignore overflow warning
np.seterr(over='ignore')

#Create folder to save plots
figure_folder = '../report/figures/'
if not os.path.exists(figure_folder):
    os.makedirs(figure_folder)


#Define functions to compute pi_hat and pi_hat_star as given in the
#question
def pi_hat_star(pi, n):
    return pi ** (n + 1)

def pi_hat(pi, n, pi_hat_star):
    return pi_hat_star(pi, n) / (pi ** n)

n = 100
pi = math.pi

#define a list of datatypes
datatype = [np.float64, np.float32, np.float16]

#generate plots by looping over all the datatypes
for dtype in datatype:   
    #define pi in current datatype
    pi_prime = dtype(pi)
    max_val = np.finfo(dtype).max
    
    #Compute n_max
    n_max = 1
    while True:
        compute = pi_prime ** n_max
        if compute > max_val:
            print compute
            break
        else:
            n_max += 1
    
    
    #plot the round off error for current datatype 
    for i in xrange(n_max - 1):
        compute_pi_hat = pi_hat(pi_prime, i, pi_hat_star)
        
        #convert pi_hat into double precision
        compute_pi_hat = np.float64(compute_pi_hat)
        epsilon = np.abs(compute_pi_hat - pi)
        plt.plot(i, epsilon, 'bo')
    
    #edit plot
    plt.ylabel("$\epsilon = |\pi - \hat{\pi}|$")
    plt.xlabel("$n$")
    
    #save plot
    figure_name = 'q1_round_off' + str(dtype) + ".pdf"
    if not os.path.exists(figure_folder + figure_name):
        plt.savefig(figure_folder + figure_name)
    
    plt.close()




