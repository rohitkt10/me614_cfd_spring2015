import os
import sys
import numpy as np
import scipy.sparse as scysparse
from pdb import set_trace as keyboard
from time import sleep
import spatial_discretization
import scipy.sparse as scysparse
import scipy.sparse.linalg as spysparselinalg  # sparse linear algebra
import scipy.linalg as scylinalg               # non-sparse linear algebra
import pylab as plt
from matplotlib import rc as matplotlibrc

machine_epsilon = np.finfo(float).eps

### this example script is hard-coded for periodic problems

#########################################
############### User Input ##############

Nx  = 128
Lx  = 1.
CFL = 0.8    # Courant-Friedrichs-Lewy number (i.e. dimensionless time step)
c_x_ref = 1
c_x   = 0.*c_x_ref  # (linear) convection speed
alpha = 0.        # diffusion coefficients
Tf  = Lx/c_x_ref # one complete cycle

## Time Advancement
#time_advancement = "Explicit-Euler"
time_advancement = "Crank-Nicolson"

## Advection Scheme
#advection_scheme = "1st-order-upwind"
advection_scheme = "2nd-order-central"
#advection_scheme = "2nd-order-upwind"

## Diffusion Scheme
diffusion_scheme = "2nd-order-central" # always-second-order central

def u_initial(X):
    return 0.5+0.5*np.sin(2*np.pi/Lx*4.*X)*np.exp(-80.0*np.power(X-0.4*Lx,2.0)/Lx**2.) # lambda functions are better..
plot_every  = 200
ymin = 0.
ymax = 1.

#########################################
######## Preprocessing Stage ############

xx = np.linspace(0.,Lx,Nx+1)
# actual mesh points are off the boundaries x=0, x=Lx
# non-periodic boundary conditions created with ghost points
x_mesh = 0.5*(xx[:-1]+xx[1:])
dx  = np.diff(xx)[0]
dx2 = dx*dx

# for linear advection/diffusion time step is a function
# of c,alpha,dx only; we use reference limits, ballpark
# estimates for Explicit Euler
dt_max_advective = dx/(c_x+machine_epsilon)             #   think of machine_epsilon as zero
dt_max_diffusive = dx2/(alpha+machine_epsilon)
dt = CFL*np.min([dt_max_advective,dt_max_diffusive])
# unitary_float = 1.+0.1*machine_epsilon # wat ?!

# Creating identity matrix
Ieye = scysparse.identity(Nx)

# Creating first derivative
Dx = spatial_discretization.Generate_Spatial_Operators(\
                        x_mesh,advection_scheme,derivation_order=1)
# Creating second derivative
D2x2 = spatial_discretization.Generate_Spatial_Operators(\
                        x_mesh,diffusion_scheme,derivation_order=2)

# Creating A,B matrices such that: A*u^{n+1} = B*u^{n} + q
if time_advancement=="Explicit-Euler":
    A = Ieye
    B = Ieye-dt*c_x*Dx+dt*alpha*D2x2
if time_advancement=="Crank-Nicolson":
    adv_diff_Op = -dt*c_x*Dx+dt*alpha*D2x2
    A = Ieye-0.5*adv_diff_Op
    B = Ieye+0.5*adv_diff_Op

# forcing csr ordering..
A , B = scysparse.csr_matrix(A),scysparse.csr_matrix(B)

#########################################
####### Eigenvalue analysis #############

keyboard()
# T = (scylinalg.inv(A.todense())).dot(B.todense())  # A^{-1}*B
# lambdas,_ = scylinalg.eig(T)

#########################################
########## Time integration #############

u = u_initial(x_mesh) # initializing solution

# Figure settings
#matplotlibrc('text.latex', preamble='\usepackage{color}')
#matplotlibrc('text',usetex=True)
#matplotlibrc('font', family='serif')
figwidth       = 10
figheight      = 6
lineWidth      = 4
textFontSize   = 28
gcafontSize    = 30
plt.ion()      # pylab's interactive mode-on

time = 0.
it   = 0.

# Plot initial conditions
fig = plt.figure(0, figsize=(figwidth,figheight))
ax   = fig.add_axes([0.15,0.15,0.8,0.8])
plt.axes(ax)
ax.plot(x_mesh,u_initial(x_mesh),'--k',linewidth=1)
ax.text(0.7,0.9,r"$t="+"%1.5f" %time+"$",fontsize=gcafontSize,transform=ax.transAxes)
ax.grid('on',which='both')
plt.setp(ax.get_xticklabels(),fontsize=gcafontSize)
plt.setp(ax.get_yticklabels(),fontsize=gcafontSize)
ax.set_xlabel(r"$x$",fontsize=textFontSize)
ax.set_ylabel(r"$u(x,t)$",fontsize=textFontSize,rotation=90)
ax.set_ylim([ymin,ymax])
plt.draw()

_ = raw_input("Plotting Initial Conditions. Press Key to start time integration")

while time < Tf:

   it   += 1
   time += dt
   
   # Update solution
   # solving : A*u^{n+1} = B*u^{n} + q
   # where q is zero for periodic and zero source terms
   u = spysparselinalg.spsolve(A,B.dot(u))
   # this operation is repeated many times.. you should
   # prefactorize 'A' to speed up computation.

   if ~bool(np.mod(it,plot_every)): # plot every plot_every time steps
       plt.cla()
       ax.plot(x_mesh,u,'-k',linewidth=lineWidth)
       ax.plot(x_mesh,u_initial(x_mesh),'--k',linewidth=1)
       ax.text(0.7,0.9,r"$t="+"%1.5f" %time+"$",fontsize=gcafontSize,transform=ax.transAxes)
       ax.grid('on',which='both')
       plt.setp(ax.get_xticklabels(),fontsize=gcafontSize)
       plt.setp(ax.get_yticklabels(),fontsize=gcafontSize)
       ax.set_xlabel(r"$x$",fontsize=textFontSize)
       ax.set_ylabel(r"$u(x,t)$",fontsize=textFontSize,rotation=90)
       ax.set_ylim([ymin,ymax])
       plt.draw()
       #       plt.tight_layout()
       #       plt.tight_layout()
       sleep(0.01)


_ = raw_input("Simulation Finished. Press Enter to continue...")
plt.close()

