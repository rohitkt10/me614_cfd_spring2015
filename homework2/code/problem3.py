#!/usr/bin/env python

import os
import sys
import numpy as np
import scipy.sparse as scysparse
from pdb import set_trace as keyboard
from time import sleep
import spatial_discretization
import scipy.sparse as scysparse
import scipy.sparse.linalg as spysparselinalg  # sparse linear algebra
import scipy.linalg as scylinalg               # non-sparse linear algebra
import pylab as plt
from matplotlib import rc as matplotlibrc
import matplotlib.pyplot as plt
from spatial_and_temporal import *
from _runge_kutta import *


#Create folder to save plots
figure_folder = '../report/figures/'
if not os.path.exists(figure_folder):
    os.makedirs(figure_folder)
    

#grid details
N = 20
L = 1.
mesh = np.linspace(0., L, N + 1)
mesh = 0.5 * (mesh[:-1] + mesh[1:])
dx = mesh[1] - mesh[0]

#advection schemes
advection_schemes = ['second order central', 'fourth order central', 'sixth order central']

#Runge Kutta Scheme orders
rk_orders = ['second_order', 'third_order', 'fourth order']

#advection speed
ad_speed = np.arange(0.1, 0.81, 0.01)

#time step
dt = 0.1


def sigma_root(T = None, dt = 0.01):
    assert T != None
    rho = np.max(np.abs(np.linalg.eigvals(T)))
    #print rho
    return (1. + rho * dt)


for i, advection_scheme in enumerate(advection_schemes):
    for j, rk_order in enumerate(rk_orders):
        for c in ad_speed:
            cfl_c = (dt / dx) * c
            
            #get R matrix
            D = advection_scheme_operator(scheme = advection_scheme, N = N)
            R = -c * D
            
            #get A, B and T matrices
            A, B = a_and_b_matrices(rk_order = rk_order, N = N, R = R, dt = dt)
            T = np.linalg.solve(A, B)
            
            #Get sigma root
            sr = sigma_root(T = T, dt = dt)
            
            plt.plot(cfl_c, sr, 'bo')
        plt.title('q3_' + str(advection_scheme) + '_' + str(rk_order) + '_sigma root vs $C_c$')
        plt.xlabel('$C_c$')
        plt.ylabel('$\sigma$ root')
        plt.savefig(figure_folder + 'q3_' + str(advection_scheme) + '_' + str(rk_order) + '_sigma root vs C_c.pdf')
        plt.close()

        
    

# initial conditions 
def u_initial(mesh):
    return (0.5 * np.sin(2* np.pi * mesh + 0.5) - 0.7 * np.cos(2. * np.pi * mesh - 1.2))






