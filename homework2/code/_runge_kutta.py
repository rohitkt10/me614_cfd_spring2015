#!/usr/bin/env python
import os
import sys
import numpy as np
from pdb import set_trace as keyboard
from time import sleep
import spatial_discretization
import scipy.sparse as scysparse
import scipy.sparse.linalg as spysparselinalg  # sparse linear algebra
import scipy.linalg as scylinalg               # non-sparse linear algebra
import pylab as plt

__all__ = ['second_order', 'third_order', 'fourth_order', 'a_and_b_matrices']

def second_order(dt = 0.1, n = 1, u_0 = None, t_0 = 0., R = None):
    """
    Routine to implement 2nd order Runge Kutta time stepping 
    """
    assert u_0 != None and R != None
    u_n = u_0
    for i in xrange(n):
        k_1 = np.dot(R, u_n)
        k_2 = np.dot(R, u_n + (dt * k_1))
        u_n = u_n + dt * (0.5 * k_1 + 0.5 * k_2)
    return u_n


def third_order(dt = 0.1, n = 1, u_0 = None, t_0 = 0., R = None):
    """
    Routine to implement 3rd order Runge Kutta time stepping
    """
    assert u_0 != None and R != None
    u_n = u_0
    for i in xrange(n):
        k_1 = np.dot(R, u_n)
        k_2 = np.dot(R, u_n + ((dt / 2.) * k_1))
        k_3 = np.dot(R, u_n - (dt * k_1) + (2 * dt * k_2))
        u_n = u_n + (dt / 6.)* (k_1 + (4. * k_2) + k_3)
    return u_n

def fourth_order(dt = 0.1, n = 1, u_0 = None, t_0 = 0., R = None):
    """
    Routine to implement 4th order Runge Kutta time stepping
    """
    assert u_0 != None and R != None
    u_n = u_0
    for i in xrange(n):
        k_1 = np.dot(R, u_n)
        k_2 = np.dot(R, u_n + ((dt / 2.) * k_1))
        k_3 = np.dot(R, u_n + ((dt / 2.) * k_2))
        k_4 = np.dot(R, u_n + (dt * k_2))
        u_n = u_n + (dt / 6.)* (k_1 + (2. * k_3) + (2. * k_2) + k_3)
    return u_n

#generate A and B matrices
def a_and_b_matrices(rk_order = 'second order', N = 10, R = None, dt = 0.01):
    """
    Routine to return A and B matrices given the order of
    the Runge Kutta Scheme used.
    """
    assert R != None
    if rk_order is 'second order':
        A = np.eye(N)
        B = np.eye(N) + ((dt/2.)*((2*R)+(dt*(R**2))))
    
    elif rk_order is 'third order':
        A = np.eye(N)
        B = np.eye(N) + ((dt/6.)*(R + (4*(R+(R*dt*(R/2.)))) + (R - ((R**2)*dt) + (2*(R+(R*dt*(R/2.)))*dt))))
    
    else:
        A = np.eye(N)
        B = np.eye(N) +((dt/6.)* (R +(2. * ( R + ( R * dt *((R * dt * (R/2.)))/2.)) + \
                                 (2. * ( R + (R * dt * (R/2.)))) + \
        ( R + ( R * dt *((R * dt * (R/2.)))/2.)))))
    
    return A, B


