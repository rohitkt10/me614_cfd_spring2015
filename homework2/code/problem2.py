"""
Solve advection diffusion problem

Time advancement schemes:

1. Explicit Euler
2. Crank Nicholson
3. Implicit Euler

Advection Schemes :

1. 2nd order Upwind
2. 4th order Upwind
3. 2nd order Central
4. 4th order Central

Diffusion Schemes :

Only 2nd order Central
"""
import os
import sys
import numpy as np
import scipy.sparse as scysparse
from pdb import set_trace as keyboard
from time import sleep
import spatial_discretization
import scipy.sparse as scysparse
import scipy.sparse.linalg as spysparselinalg  # sparse linear algebra
import scipy.linalg as scylinalg               # non-sparse linear algebra
import pylab as plt
from matplotlib import rc as matplotlibrc
import matplotlib.pyplot as plt
from spatial_and_temporal import *

#Extract Pi
pi = np.pi


#Create folder to save plots
figure_folder = '../report/figures/'
if not os.path.exists(figure_folder):
    os.makedirs(figure_folder)

#Create dictionaries to define advection schemes, times advancement schemes
#and diffusion schemes
advection_schemes = ['second order upwind', 'fourth order upwind',
                     'second order central', 'fourth order central']

diffusion_scheme = 'second order central'

time_schemes = ['explicit euler', 'crank nicholson', 'implicit euler']


#Mesh details
N = 10
L = 1.
mesh = np.linspace(0, L, N + 1)
mesh = 0.5 * (mesh[:-1] + mesh[1:])
dx = mesh[1] - mesh[0]
dt = 0.01
time_steps = 1000

#advection and diffusion constants
c = 2.  #advection speed
alpha = 0.8 #diffusivity
cfl_c = (c * dt) / dx
cfl_d = (alpha * dt) / (dx ** 2)

#Solution parameters
c1 = 0.5
w1 = 2. * pi
y1 = -0.5
c2 = 0.7
w2 = 2. * pi
y2 = 1.2

#time integration
def time_integrate(time_scheme = 'explicit euler',
                   advection_scheme = 'second order central',
                   u_0 = None, 
                   n = 1,
                   cfl_c = 0.8,
                   cfl_d = 0.8,
                   c = 1.,
                   N = 10):
    """
    Integrating the solution in time.
    
    ::u_0:: Initial condition
    ::n:: number of time steps
    ::dt:: time step
    """
    assert u_0 != None
    A = A_matrix(time_scheme = time_scheme, advection_scheme = advection_scheme, cfl_c = cfl_c, cfl_d = cfl_d, N = N, c = c)
    B = B_matrix(time_scheme = time_scheme, advection_scheme = advection_scheme, cfl_c = cfl_c, cfl_d = cfl_d, N = N, c = c)
    T = generate_transition_matrix(A, B)
    u_n = u_0
    for i in xrange(n):
        u_n = np.dot(T, u_n)
    return u_n
        


#initial conditions 
def u_initial(mesh):
    """
    Initial conditions
    """
    return (c1 * np.sin(w1 * mesh - y1) - c2 * np.cos(w2 * mesh - y2))


#Analytical solution
def u_analytical(mesh, t = 0.):
    """
    Analytical solution
    """
    return c1 * np.exp(- (w1 ** 2) * alpha * t) * np.sin((w1 * (mesh - (c * t)) - y1)) \
    - c2 * np.exp(- (w2 ** 2) * alpha * t) * np.cos((w2 * (mesh - (c * t)) - y2))


plot_spy = False
iso_contour = False
validation = False
rms_const_dt = True
rms_const_dx = True


if plot_spy:
    #Plotting all the spys of the transition matrices for different time advancement schemes 
    for scheme in time_schemes:
        for ad_scheme in advection_schemes:
            if 'implicit' in scheme:
                #Implicit Euler Time advancement
                A = A_matrix(time_scheme = scheme, advection_scheme = ad_scheme, cfl_c = cfl_c, cfl_d = cfl_d, N = N, c = c)
                B = B_matrix(time_scheme = scheme, advection_scheme = ad_scheme, cfl_c = cfl_c, cfl_d = cfl_d, N = N, c = c)
            
            elif 'explicit' in scheme:
                #Explicit Euler Time advancement
                A = A_matrix(time_scheme = scheme, advection_scheme = ad_scheme, cfl_c = cfl_c, cfl_d = cfl_d, N = N, c = c)
                B = B_matrix(time_scheme = scheme, advection_scheme = ad_scheme, cfl_c = cfl_c, cfl_d = cfl_d, N = N, c = c)
            
            else:
                #Crank Nicholson Time advancement
                A = A_matrix(time_scheme = scheme, advection_scheme = ad_scheme, cfl_c = cfl_c, cfl_d = cfl_d, N = N, c = c)
                B = B_matrix(time_scheme = scheme, advection_scheme = ad_scheme, cfl_c = cfl_c, cfl_d = cfl_d, N = N, c = c)
            
            sA = scysparse.csr_matrix(A)
            sB = scysparse.csr_matrix(B)
            if ad_scheme is 'second order central':
                plt.spy(sA, marker = 'o')
                plt.savefig(figure_folder + 'q2_spy_A_' + scheme + '_' + ad_scheme + '.pdf')
                plt.close()
                plt.spy(sB, marker = 'o')
                plt.savefig(figure_folder + 'q2_spy_B_' + scheme + '_' + ad_scheme + '.pdf')
                plt.close()
            
            T = generate_transition_matrix(A, B)
            eig_T = np.abs(np.linalg.eigvals(T))
            #if scheme is 'implicit euler':
            #    print eig_T
            


u_0 = u_initial(mesh)
time_steps = np.arange(1, 11)
#for time_step in time_steps:
#    u_10 = time_integrate(time_scheme = 'explicit euler', advection_scheme = 'second order central',
#                          u_0 = u_0, n = time_step, cfl_c = cfl_c, cfl_d = cfl_d, c = c)
#    plt.plot(u_0, 'r--')
#    plt.plot(u_10, 'b-')
#    plt.show()
#exit()

#isocontour
if iso_contour:
    cfl_c = np.linspace(0., 1., 100)
    cfl_d = np.linspace(0. ,1., 100)
    CFL_C, CFL_D = np.meshgrid(cfl_c, cfl_d)
    N = 10
    dx = 0.1
    L = (N - 1) * dx
    mesh = np.linspace(0, L, N)
    
    

#validation
if validation:
    L = 1.
    tf = 1. / (w2 ** 2 * alpha)
    grid_density = [20, 80, 200]
    time_steps = np.linspace(0, tf, 10)
    dt = time_steps[1] - time_steps[0]
    for time_scheme in time_schemes:
        for advection_scheme in advection_schemes:
            for N in grid_density:
                mesh = np.linspace(0, L, N + 1)
                mesh = 0.5 * (mesh[:-1] + mesh[1:])
                dx = mesh[1] - mesh[0]
                u_0 = u_initial(mesh)
                cfl_c = (c * dt) / dx
                cfl_d = (alpha * dt) / (dx ** 2)
                u_tf = u_0
                for time_step in time_steps:
                    u_tf = time_integrate(time_scheme = time_scheme, advection_scheme = advection_scheme,
                                          u_0 = u_tf, cfl_c = cfl_c, cfl_d = cfl_d, c = c, N = N)
                if advection_scheme is 'second order central':
                    plt.plot(mesh, u_tf, 'r--', linewidth = 2.5, label = 'Numerical solution')
                    plt.plot(mesh, u_analytical(mesh, t = tf), 'b-', linewidth = 4, label = 'Analytical solution')
                    plt.xlabel('$x$')
                    plt.ylabel('Solution')
                    plt.title(str(time_scheme) + " ; $N=$"+str(N))
                    plt.legend(loc = 'best')
                    figure_name = 'q2_sol_' + str(time_scheme) + '_N=' + str(N) + '.pdf'
                    plt.savefig(figure_folder + figure_name)
                    plt.close()


#Root mean square error for constant dt
if rms_const_dt:
    dt = 0.01
    L = 1.
    tf = 1. / (w2 ** 2 * alpha)
    time_steps = np.arange(0, tf, 10)
    grid_density = np.linspace(10, 100, 91)
    for time_scheme in time_schemes:
        for advection_scheme in advection_schemes:
            for N in grid_density:
                mesh = np.linspace(0, L, N + 1)
                mesh = 0.5 * (mesh[:-1] + mesh[1:])
                dx = mesh[1] - mesh[0]
                u_0 = u_initial(mesh)
                cfl_c = (c * dt) / dx
                cfl_d = (alpha * dt) / (dx ** 2)
                u_tf = u_0
                for time_step in time_steps:
                    u_tf = time_integrate(time_scheme = time_scheme, advection_scheme = advection_scheme,
                                          u_0 = u_tf, cfl_c = cfl_c, cfl_d = cfl_d, c = c, N = N)
                
                u_a = u_analytical(mesh, t = tf)
                error = np.sqrt((1. / N) * np.sum(np.square(u_a - u_tf)))
                plt.plot(N, error, 'bo')
            plt.title('RMSE vs $Delta$x ; constant $dt$')
            plt.xlabel('$Delta$x')
            plt.ylabel('RMSE')
            plt.savefig(figure_folder + 'q2_' + str(time_scheme) + '_' + str(advection_scheme) + '_RMSE vs Deltax.pdf')
            plt.close()

if rms_const_dx:
    L = 1.
    N = 10
    mesh = np.linspace(0, L, N + 1)
    mesh = 0.5 * (mesh[:-1] + mesh[1:])
    dx = mesh[1] - mesh[0]
    tf = 1. / (w2 ** 2 * alpha)
    time_steps = np.linspace(5, 30, 26)
    print time_steps
    grid_density = np.linspace(10, 100, 91)
    for time_scheme in time_schemes:
        for advection_scheme in advection_schemes:
            for ts in time_steps:
                u_0 = u_initial(mesh)
                dt = tf / ts
                cfl_c = (c * dt) / dx
                cfl_d = (alpha * dt) / (dx ** 2)
                u_tf = u_0
                for i in xrange(int(ts)):
                    u_tf = time_integrate(time_scheme = time_scheme, advection_scheme = advection_scheme,
                                          u_0 = u_tf, cfl_c = cfl_c, cfl_d = cfl_d, c = c, N = N)
                
                u_a = u_analytical(mesh, t = tf)
                error = np.sqrt((1. / N) * np.sum(np.square(u_a - u_tf)))
                plt.plot(ts, error, 'bo')
            plt.title('RMSE vs $Delta$x ; constant $dt$')
            plt.xlabel('$Delta$x')
            plt.ylabel('RMSE')
            plt.savefig(figure_folder + 'q2_' + str(time_scheme) + '_' + str(advection_scheme) + '_RMSE vs Deltat.pdf')
            plt.close()
    



