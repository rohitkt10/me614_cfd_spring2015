import sys
import numpy as np
import scipy
import pylab as plt
from pdb import set_trace as keyboard
from matplotlib import rc as matplotlibrc
from math import sqrt
import matplotlib
from latexify import latexify
"""
def latexify(fig_width=None, fig_height=None, columns=1):
    Set up matplotlib's RC params for LaTeX plotting.
    Call this before plotting a figure.

    Parameters
    ----------
    fig_width : float, optional, inches
    fig_height : float,  optional, inches
    columns : {1, 2}
    

    # code adapted from http://www.scipy.org/Cookbook/Matplotlib/LaTeX_Examples

    # Width and max height in inches for IEEE journals taken from
    # computer.org/cms/Computer.org/Journal%20templates/transactions_art_guide.pdf

    assert(columns in [1,2])

    if fig_width is None:
        fig_width = 3.39 if columns==1 else 6.9 # width in inches

    if fig_height is None:
        golden_mean = (sqrt(5)-1.0)/2.0    # Aesthetic ratio
        fig_height = fig_width*golden_mean # height in inches

    MAX_HEIGHT_INCHES = 8.0
    if fig_height > MAX_HEIGHT_INCHES:
        print("WARNING: fig_height too large:" + fig_height + 
              "so will reduce to" + MAX_HEIGHT_INCHES + "inches.")
        fig_height = MAX_HEIGHT_INCHES

    params = {'backend': 'ps',
              'text.latex.preamble': ['\usepackage{gensymb}'],
              'axes.labelsize': 8, # fontsize for x and y labels (was 10)
              'axes.titlesize': 8,
              'text.fontsize': 8, # was 10
              'legend.fontsize': 8, # was 10
              'xtick.labelsize': 8,
              'ytick.labelsize': 8,
              'text.usetex': True,
              'figure.figsize': [fig_width,fig_height],
              'font.family': 'serif'
    }

    matplotlib.rcParams.update(params)

# Extracting PI constant
PI = np.pi

# LaTeX setup
matplotlibrc('text.latex', preamble='\usepackage{color}')
matplotlibrc('text',usetex=True)
matplotlibrc('font', family='serif')
"""

latexify()

# Extracting PI constant
PI = np.pi


figure_folder = "../report/"


Nx = 101
x = np.linspace(0,2*PI,Nx)
y_cos = np.cos(x)
y_sin = np.sin(x)

Plot_CosineAndSine = 'y'
Plot_SomethingElse = 'y'

if Plot_CosineAndSine == 'y':
    
    figure_name = "cosine_and_sine.pdf"
  
    figwidth       = 18
    figheight      = 6
    """
    lineWidth      = 3
    textFontSize   = 28
    gcafontSize    = 30
    """
    fig = plt.figure(0, figsize=(figwidth,figheight))
    ax_left   = fig.add_subplot(1,2,1)
    ax_right  = fig.add_subplot(1,2,2)
    
    ax = ax_left
    plt.axes(ax)
    ax.plot(x,y_cos,'-r')
    #plt.setp(ax.get_xticklabels(),fontsize=gcafontSize)
    #plt.setp(ax.get_yticklabels(),fontsize=gcafontSize)
    ax.grid('on',which='both')
    #  ax.set_xticks()
    #  ax.set_xlim()
    #  ax.set_yticks()
    #  ax.set_ylim()
    ax.set_xlabel(r"$x$ axis")#,fontsize=textFontSize)
    ax.set_ylabel(r"$y$ axis")#,fontsize=textFontSize,rotation=90)
    ax = ax_right
    plt.axes(ax)
    ax.plot(x,y_sin,'-k')#,linewidth=lineWidth)
    #plt.setp(ax.get_xticklabels(),fontsize=gcafontSize)
    #plt.setp(ax.get_yticklabels(),fontsize=gcafontSize)
    ax.grid('on',which='both')
    ax.set_xlabel(r"$x$ axis")#,fontsize=textFontSize)
    ax.set_ylabel(r"$y$ axis")#,fontsize=textFontSize,rotation=90)
    #print '---'
    #exit()

    #figure_file_path = figure_folder + figure_name
    #print '---'
    #exit()
    #print figure_file_path
    #exit()
    #print "Saving figure: " + figure_file_path
    #print '******'
    #exit()
    plt.tight_layout()
    print '*************'
    #exit()
    plt.savefig("homework0/report/image.pdf")
    plt.close()

if Plot_SomethingElse == 'n':

  figure_name = "other_figure.pdf"
  # Put here other plots


 
