#!/usr/bin/env python

import os
import sys
import numpy as np
import scipy.sparse as scysparse
from pdb import set_trace as keyboard
from time import sleep
import spatial_operators
import scipy.sparse as scysparse
import scipy.sparse.linalg as spysparselinalg  # sparse linear algebra
import scipy.linalg as scylinalg               # non-sparse linear algebra
import pylab as plt
from matplotlib import rc as matplotlibrc
from datetime import datetime

time_adv_type = ['explicit', 'implicit']
time_advancement_list = ['RK1', 'RK2', 'RK3', 'RK4']

def time_advance_explicit(U, Rx, Nxc, time_advancement = 'RK1', dt = 0.0002, tol = 0.001):
    """
    Explicit time advancement for both convective and diffusive terms
    """
    if time_advancement == 'RK1': 
        for i in xrange(1,Nxc+1):
            for j in xrange(1,Nxc+1):
                U[i][j] = U[i][j] + dt*(Rx[i, j])
                
    if time_advancement == 'RK2':
        for i in xrange(1, Nxc + 1):
            for j in xrange(1, Nxc + 1):
                if np.abs(U[i, j]) > tol:
                    Rux = Rx[i, j] / U[i, j]
                    k1 = Rx[i, j]
                    k2 = Rux * (U[i, j] + dt * k1)
                    U[i, j] = U[i, j] + (dt / 2.) * (k1 + k2)
    
    
    if time_advancement == 'RK3':
        for i in xrange(1, Nxc + 1):
            for j in xrange(1, Nxc + 1):
                if np.abs(U[i, j]) > tol:
                    Rux = Rx[i, j] / U[i, j]
                    k1 = Rx[i, j]
                    k2 = Rux * (U[i, j] + (dt/2) * k1)
                    k3 = Rux * (U[i, j] + dt * k2)
                    U[i, j] = U[i, j] + (dt / 6.) * (k1 + 4 * k2 + k3)
    
    
    if time_advancement == 'RK4':
        for i in xrange(1, Nxc + 1):
            for j in xrange(1, Nxc + 1):
                if np.abs(U[i, j]) > tol:
                    Rux = Rx[i, j] / U[i, j]
                    k1 = Rx[i, j]
                    k2 = Rux * (U[i, j] + (dt/2) * k1)
                    k3 = Rux * (U[i, j] + (dt/2) * k2)
                    k4 = Rux * (U[i, j] + dt * k3)
                    U[i, j] = U[i, j] + (dt / 6.) * (k1 + 2 * k2 + 2 * k3 + k4)    
    
    return U


def time_advance_semi_implicit(U, Rc, Rd, Nxc, time_advancement = 'RK1', dt = 0.0002, tol = 0.001):
    """
    Explicit time advancement for convective and crank nicholson time
    advancement(semi-implicit) for diffusive terms
    """
    if time_advancement == 'RK1':
        for i in xrange(1,Nxc+1):
            for j in xrange(1,Nxc+1):
                if np.abs(U[i, j]) > tol:
                    Rcx = Rc[i, j] / U[i, j]
                    Rdx = Rd[i, j] / U[i, j]
                    A = 1 - (dt / 2.) * Rdx
                    B = 1 + dt*Rcx + ((dt / 2.) * Rdx)
                    U[i, j] = (B / A) * U[i, j]
                
    if time_advancement == 'RK2':
        for i in xrange(1, Nxc + 1):
            for j in xrange(1, Nxc + 1):
                if np.abs(U[i, j]) > tol:
                    Rcx = Rc[i, j] / U[i, j]
                    Rdx = Rd[i, j] / U[i, j]
                    k1 = Rcx
                    k2 = Rcx * (1 + dt * k1)
                    A = 1 - (dt / 2.) * Rdx
                    B = 1 + (dt / 2) * (k1 + k2 + Rdx)
                    U[i, j] = (B / A) * U[i, j]
    
    
    if time_advancement == 'RK3':
        for i in xrange(1, Nxc + 1):
            for j in xrange(1, Nxc + 1):
                if np.abs(U[i, j]) > tol:
                    Rcx = Rc[i, j] / U[i, j]
                    Rdx = Rd[i, j] / U[i, j]
                    k1 = Rcx
                    k2 = Rcx * (1 + (dt/2) * k1)
                    k3 = Rcx * (1 + dt * k2)
                    A = 1 - (dt / 2.) * Rdx
                    B = 1 + (dt / 6) * (k1 + 4 * k2 + k3) + (dt / 2.) * (Rdx)
                    U[i, j] = (B / A) * U[i, j]
    
    
    if time_advancement == 'RK4':
        for i in xrange(1, Nxc + 1):
            for j in xrange(1, Nxc + 1):
                if np.abs(U[i, j]) > tol:
                    Rcx = Rc[i, j] / U[i, j]
                    Rdx = Rd[i, j] / U[i, j]
                    k1 = Rcx
                    k2 = Rcx * (1 + (dt/2) * k1)
                    k3 = Rcx * (1 + (dt/2) * k2)
                    k4 = Rcx * (1 + dt * k3)
                    A = 1 - (dt / 2.) * Rdx
                    B = 1 + (dt / 6) * (k1 + 2 * k2 + 2 * k3 + k4) + (dt / 2.) * (Rdx)
                    U[i, j] = (B / A) * U[i, j]
    
    return U
    
    