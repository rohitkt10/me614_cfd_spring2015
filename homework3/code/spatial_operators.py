import numpy as np
import scipy.sparse as scysparse
import sys
from pdb import set_trace as keyboard
import scipy.sparse as scysparse
import scipy.sparse.linalg as spysparselinalg
import scipy.linalg as scylinalg        # non-sparse linear algebra
import matplotlib.pyplot as plt 

############################################################
############################################################


#Routine to create the divergence operator 
def create_DivGrad_operator(Dxc = None, Dyc = None,
                            Xc = None,
                            Yc = None,
                            pressureCells_Mask = None,
                            boundary_conditions="Homogeneous Neumann"):
     # defaults to "Homogeneous Dirichlet"
     possible_boundary_conditions = ["Homogeneous Dirichlet","Homogeneous Neumann", "Periodic"]

     if not(boundary_conditions in possible_boundary_conditions):
         sys.exit("Boundary conditions need to be either: " +
                  repr(possible_boundary_conditions))
         
     
     
     # numbering with -1 means that it is not a fluid cell (i.e. either ghost cell or external)
     numbered_pressureCells = -np.ones(Xc.shape,dtype='int64')
     
     #get row and column numbers of all the pressure cells not masked out
     jj_C,ii_C = np.where(pressureCells_Mask==True)
     
     # total number of pressure nodes, not necessarily equal to Nxc*Nyc
     Np = len(jj_C)
     #exit()
     DivGrad = scysparse.lil_matrix((Np,Np),dtype="float64")

     #number all the pressure cells(row major style)
     numbered_pressureCells[jj_C,ii_C] = range(0,Np) # automatic numbering done via 'C' flattening
     
     Nxc = Xc.shape[0] - 2    
     Nyc = Xc.shape[1] - 2
     delxc = Xc[0, 0] - Xc[0, 1]
     delyc = Xc[0, 0] - Xc[0, 1]
     delxe = Xc[0, 0] - Xc[0, 1]
     delxw = Xc[0, 0] - Xc[0, 1]
     delyn = Xc[0, 0] - Xc[0, 1]
     delys = Xc[0, 0] - Xc[0, 1]
     if boundary_conditions == "Periodic":
          numbered_pressureCells[0, :] = numbered_pressureCells[Nxc, :]
          numbered_pressureCells[Nxc + 1, :] = numbered_pressureCells[1, :]
          numbered_pressureCells[:, 0] = numbered_pressureCells[:, Nyc]
          numbered_pressureCells[:, Nyc + 1] = numbered_pressureCells[:, 1]
          
          row = 0
          for i in xrange(1, Nxc + 1):
               for j in xrange(1, Nyc + 1):
                    n_c = numbered_pressureCells[i, j]
                    n_e = numbered_pressureCells[i, j + 1]
                    n_w = numbered_pressureCells[i, j - 1]
                    n_n = numbered_pressureCells[i + 1, j]
                    n_s = numbered_pressureCells[i - 1, j]
                    DivGrad[row, n_c] = -(1. / (delxc * delxe)) - (1. / (delxc * delxw)) - (1. / (delyc * delyn)) - (1. / (delyc * delys))
                    DivGrad[row, n_e] = 1. / (delxc * delxe)
                    DivGrad[row, n_w] = 1. / (delxc * delxw)
                    DivGrad[row, n_n] = 1. / (delyc * delyn)
                    DivGrad[row, n_s] = 1. / (delyc * delys)
                    row += 1
          return DivGrad
     
     #get delta X_c, delta X_E, delta X_W, delta X_N and delta X_S
     inv_DxC = 1./Dxc[jj_C,ii_C]
     inv_DyC = 1./Dyc[jj_C,ii_C]
     inv_DxE = 1./(Xc[jj_C,ii_C+1]-Xc[jj_C,ii_C])
     inv_DyN = 1./(Yc[jj_C+1,ii_C]-Yc[jj_C,ii_C])
     inv_DxW = 1./(Xc[jj_C,ii_C]-Xc[jj_C,ii_C-1])
     inv_DyS = 1./(Yc[jj_C,ii_C]-Yc[jj_C-1,ii_C])
     
     #define a sparse matrix to store the Laplacian Operator
     DivGrad = scysparse.lil_matrix((Np,Np),dtype="float64") # initialize with all zeros
     #Grad = scysparse.lil_matrix((Np,Np),dtype="float64")
     #Div = scysparse.lil_matrix((Np,Np),dtype="float64")
     
     #get cell locations for pressure cells and velocity cells
     iC = numbered_pressureCells[jj_C,ii_C]
     iE = numbered_pressureCells[jj_C,ii_C+1]
     iW = numbered_pressureCells[jj_C,ii_C-1]
     iS = numbered_pressureCells[jj_C-1,ii_C]
     iN = numbered_pressureCells[jj_C+1,ii_C]

     # consider pre-multiplying all of the weights by the local value of dx*dy

     # start by creating operator assuming homogeneous Neumann
     ## if east node is inside domain
     east_node_mask = (iE!=-1)
     ii_center = iC[east_node_mask]
     ii_east   = iE[east_node_mask]
     inv_dxc_central = inv_DxC[ii_center]
     inv_dxc_east    = inv_DxE[ii_center]
     DivGrad[ii_center,ii_east]   += inv_dxc_central*inv_dxc_east
     DivGrad[ii_center,ii_center] -= inv_dxc_central*inv_dxc_east
     
     ## if west node is inside domain
     west_node_mask = (iW!=-1)
     ii_center  = iC[west_node_mask]
     ii_west    = iW[west_node_mask]
     inv_dxc_central = inv_DxC[ii_center]
     inv_dxc_west    = inv_DxW[ii_center]
     DivGrad[ii_center,ii_west]   += inv_dxc_central*inv_dxc_west
     DivGrad[ii_center,ii_center] -= inv_dxc_central*inv_dxc_west
     

     ## if north node is inside domain
     north_node_mask = (iN!=-1)
     ii_center  = iC[north_node_mask]
     ii_north   = iN[north_node_mask]
     inv_dyc_central  = inv_DyC[ii_center]
     inv_dyc_north    = inv_DyN[ii_center]
     DivGrad[ii_center,ii_north]   += inv_dyc_central*inv_dyc_north
     DivGrad[ii_center,ii_center]  -= inv_dyc_central*inv_dyc_north
     

      ## if south node is inside domain
     south_node_mask = (iS!=-1)
     ii_center  = iC[south_node_mask]
     ii_south   = iS[south_node_mask]
     inv_dyc_central  = inv_DyC[ii_center]
     inv_dyc_south    = inv_DyS[ii_center]
     DivGrad[ii_center,ii_south]   += inv_dyc_central*inv_dyc_south
     DivGrad[ii_center,ii_center]  -= inv_dyc_central*inv_dyc_south
     
     
     # if Dirichlet boundary conditions are requested, need to modify operator
     if boundary_conditions == "Homogeneous Dirichlet":

          # for every east node that is 'just' outside domain
          east_node_mask = (iE == -1) & (iC != -1)
          ii_center = iC[east_node_mask]
          inv_dxc_central = inv_DxC[ii_center]
          inv_dxc_east    = inv_DxE[ii_center]
          DivGrad[ii_center,ii_center]  -= 2.*inv_dxc_central*inv_dxc_east
          

          # for every west node that is 'just' outside domain
          west_node_mask = (iW==-1)&(iC!=-1)
          ii_center = iC[west_node_mask]
          inv_dxc_central = inv_DxC[ii_center]
          inv_dxc_west    = inv_DxW[ii_center]
          DivGrad[ii_center,ii_center]  -= 2.*inv_dxc_central*inv_dxc_west

          # for every north node that is 'just' outside domain
          north_node_mask = (iN==-1)&(iC!=-1)
          ii_center = iC[north_node_mask]
          inv_dyc_central  = inv_DyC[ii_center]
          inv_dyc_north    = inv_DyN[ii_center]
          DivGrad[ii_center,ii_center]  -= 2.*inv_dyc_central*inv_dyc_north

          # for every south node that is 'just' outside domain
          south_node_mask = (iS==-1)&(iC!=-1)
          ii_center = iC[south_node_mask]
          inv_dyc_central  = inv_DyC[ii_center]
          inv_dyc_south    = inv_DyS[ii_center]
          DivGrad[ii_center,ii_center]  -= 2.*inv_dyc_central*inv_dyc_south


     DivGrad = DivGrad.tocsr()
     return DivGrad
