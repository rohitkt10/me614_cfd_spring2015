import os
import sys
import numpy as np
import scipy.sparse as scysparse
from pdb import set_trace as keyboard
from time import sleep
import spatial_operators
import scipy.sparse as scysparse
import scipy.sparse.linalg as spysparselinalg 
import scipy.linalg as scylinalg   
import pylab as plt
from matplotlib import rc as matplotlibrc
from datetime import datetime
from time_integration import *

#analytical solutions
def u_analytical(x, y, t):
    return -np.exp(-2 * t) * np.cos(x) * np.sin(y)

def v_analytical(x, y, t):
    return -np.exp(-2 * t) * np.cos(y) * np.sin(x)

def p_analytical(x, y, t):
    return -0.25 * np.exp(-2 * t) * (np.cos(2 * x) + np.cos(2 * y))


machine_epsilon = np.finfo(float).eps
figure_path = "../figures/"

if not os.path.exists(figure_path):
    os.makedirs(figure_path)

### this example script is hard-coded for periodic problems

#########################################
############### User Input ##############

list_of_time_advancement = ['RK1',
                            'RK2',
                            'RK3',
                            'RK4']
type_of_time_advancement = ['Fully-explicit', 'Semi-implicit']
list_of_grid_sizes = [50, 75, 100, 125]

for Nxc in xrange(5, 10):
    Nxc = 9
    #print Nxc
    Nyc = Nxc
    Np   = Nxc*Nyc
    Lx   = 2.*np.pi
    Ly   = 2.*np.pi
    xu_initial = np.linspace(0.,Lx,Nxc+1)
    yv_initial = np.linspace(0.,Ly,Nyc+1)
    xc = 0.5*(xu_initial[1:]+xu_initial[:-1])
    yc = 0.5*(yv_initial[1:]+yv_initial[:-1])
    dxc = np.diff(xc)
    dyc = np.diff(yc)
    
    
    
    x_xu = xu_initial[:-1]
    y_xu = xu_initial[:-1] + (dxc[1]/2)
    x_yv = yv_initial[:-1] + (dxc[1]/2) 
    y_yv = yv_initial[:-1]
    dx_xu = np.diff(x_xu)
    dy_xu = np.diff(y_xu)
    dx_yv = np.diff(x_yv)
    dy_yv = np.diff(y_yv)
    x_xu = np.concatenate([[x_xu[0]-dx_xu[1]],x_xu,[x_xu[-1]+dx_xu[1]]])
    y_xu = np.concatenate([[y_xu[0]-dy_xu[1]],y_xu,[y_xu[-1]+dy_xu[1]]])
    x_yv = np.concatenate([[x_yv[0]-dx_yv[1]],x_yv,[x_yv[-1]+dx_yv[1]]])
    y_yv = np.concatenate([[y_yv[0]-dy_yv[1]],y_yv,[y_yv[-1]+dy_yv[1]]])
    x_xc = np.concatenate([[xc[0]-dxc[1]],xc,[xc[-1]+dxc[1]]])
    y_yc = np.concatenate([[yc[0]-dyc[1]],yc,[yc[-1]+dyc[1]]])
    
    
    dxc = np.diff(x_xc)  # pressure-cells spacings
    dyc = np.diff(y_yc)
    
    #average grid size
    h = np.sqrt(dxc[0] * dyc[0])
    
    
    #Velocity and Pressure grid
    [Xu, Yu] = np.meshgrid(x_xu , y_xu)
    [Xv, Yv] = np.meshgrid(x_yv , y_yv)
    [Xc,Yc]   = np.meshgrid(x_xc,y_yc)
    [Dxc,Dyc] = np.meshgrid(dxc,dyc)
    
    tol = 0.001

    pressureCells_Mask = np.zeros(Xc.shape)
    pressureCells_Mask[1:-1,1:-1] = True
    numbered_pressureCells = -np.ones(Xc.shape,dtype='int64')
    j_C,i_C = np.where(pressureCells_Mask==True)
    Np = len(j_C)
    numbered_pressureCells[j_C,i_C] = range(0,Np)
    
    u_Cells_Mask = np.zeros(Xu.shape)
    u_Cells_Mask [1:-1,1:-1] = True
    numbered_uCells = -np.ones(Xu.shape,dtype='int64')
    j_U, i_U = np.where(u_Cells_Mask==True)
    Nu = len(j_U)
    numbered_uCells[j_U, i_U] = range(0,Nu)
    
    U = np.zeros(Xu.shape , dtype="float64")
    U_c = np.zeros(Xu.shape , dtype="float64")
    U[j_U, i_U] = -np.cos(x_xu[j_U])*np.sin(y_xu[i_U])
    U[1:Nxc+1, 0] = U[1:Nxc+1, Nxc]
    U[1:Nxc+1, Nxc+1] = U[1:Nxc+1,1]
    U[0,0:Nxc+2] = U[Nxc, 0:Nxc+2]
    U[Nxc + 1, 0:Nxc+2] = U[1, 0:Nxc+2]
    
    v_Cells_Mask = np.zeros(Xv.shape)
    v_Cells_Mask [1:-1,1:-1] = True
    numbered_vCells = -np.ones(Xv.shape,dtype='int64')
    j_V, i_V = np.where(v_Cells_Mask==True)
    Nv = len(j_V)
    numbered_vCells[j_V, i_V] = range(0,Nv)
    
    V = np.zeros(Xu.shape , dtype="float64")
    V_c = np.zeros(Xu.shape , dtype="float64")
    V[j_V, i_V] = np.sin(x_yv[j_V])*np.cos(y_yv[i_V])
    V[1:Nxc+1, 0] = V[1:Nxc+1, Nxc]
    V[1:Nxc+1, Nxc+1] = V[1:Nxc+1,1]
    V[0,0:Nxc+2] = V[Nxc, 0:Nxc+2]
    V[Nxc + 1, 0:Nxc+2] = V[1, 0:Nxc+2]
    
    #initial velocity
    vel_ini = np.sqrt(U**2 +V**2)
    
    #terms in convection operator 
    duudx = np.zeros(Xu.shape , dtype="float64")
    duvdy = np.zeros(Xu.shape , dtype="float64")
    duvdx = np.zeros(Xv.shape , dtype="float64")
    dvvdx = np.zeros(Xv.shape , dtype="float64")
    Rc_x = np.zeros(Xu.shape , dtype="float64")
    Rc_y = np.zeros(Xv.shape , dtype="float64")
    Sx_xx = np.zeros(Xu.shape , dtype="float64")
    Sx_xy = np.zeros(Xu.shape , dtype="float64")
    Sy_yx = np.zeros(Xv.shape , dtype="float64")
    Sy_yy = np.zeros(Xv.shape , dtype="float64")
    Rd_x = np.zeros(Xu.shape , dtype="float64")
    Rd_y = np.zeros(Xv.shape , dtype="float64")
    u_a = np.zeros(Xu.shape , dtype="float64")
    v_a = np.zeros(Xv.shape , dtype="float64")
    delu = np.zeros(Xu.shape , dtype="float64")
    delv = np.zeros(Xv.shape , dtype="float64")
    Rx = np.zeros(Xu.shape , dtype="float64")
    Ry = np.zeros(Xv.shape , dtype="float64")
    #Rux = np.zeros(Xu.shape , dtype="float64")
    #Rvy = np.zeros(Xv.shape , dtype="float64")
    
    #tf counts the time, T is the max. time, dt is the time step and n
    #is the number of iterations
    tf = 0.
    T = 2
    dt = 0.0002
    n = 0
    
    #create a DivGrad operator
    DivGrad = spatial_operators.create_DivGrad_operator(Dxc,Dyc,Xc,Yc,pressureCells_Mask,boundary_conditions="Periodic")
    
    #Get solution at time T = 2
    while True:
        # calculation of U* , V*
        for i in xrange(1,Nxc+1):
            for j in xrange(1,Nxc+1):
                    #del(uu)/del(x) term
                    duudx[i, j] = (((U[i, j]+U[i, j+1])/2)**2 - ((U[i, j]+U[i, j-1])/2)**2)/dxc[1]        
    
                    #del(uv)/del(x) term
                    duvdy[i, j] = (((U[i, j]+U[i+1, j])/2)*((V[i+1, j]+V[i+1, j-1])/2) -                  
                                    ((U[i, j]+U[i-1, j])/2)*((V[i, j]+V[i, j-1])/2))/dxc[1]
    
                    #x momentum convextive term
                    Rc_x[i, j] = duudx[i, j] + duvdy[i, j]
    
                    #del(uv)/dx term
                    duvdx[i, j] = (((U[i, j+1] + U[i-1, j+1])/2)*((V[i, j]+V[i, j-1])/2)-                
                                    ((U[i, j]+U[i-1, j])/2)*((V[i, j]+V[i, j-1])/2))/dyc[1]
                    #del(vv)/dy term
                    dvvdx[i, j] = (((V[i+1, j]+V[i, j])/2)**2 - ((V[i, j]+V[i-1, j])/2)**2)/dyc[1]
    
                    #y momentum convextive terms
                    Rc_y[i, j] = duvdx[i, j] + dvvdx[i, j]
    
                    #strain rates - x direction
                    Sx_xx[i, j] = (2*((U[i, j+1]-U[i, j])/dxc[1])-2*((U[i, j]-U[i, j-1])/dxc[1]))/dxc[1] 
                    Sx_xy[i, j] = ((((U[i+1, j]-U[i, j])/dyc[1]) + ((V[i+1, j]-V[i+1, j-1])/dxc[1]))
                                        - (((U[i, j]-U[i-1, j])/dyc[1]) + ((V[i, j]-V[i, j-1])/dxc[1])))
    
                    #x diffusiion
                    Rd_x[i, j] = Sx_xx[i, j] + Sx_xy[i, j]
    
                    #strain rates - y diffusion
                    Sy_yx[i, j] = ((((V[i, j+1]-V[i, j])/dxc[1])+((U[i, j+1]-U[i-1, j+1])/dyc[1]))
                                        - (((V[i, j]-V[i, j-1])/dxc[1])+((U[i, j]-U[i-1, j])/dyc[1])))/dxc[1]
                    Sy_yy[i, j] = (((V[i+1, j]-V[i, j])/dyc[1])-((V[i, j]-V[i-1, j])/dyc[1]))/dyc[1]
    
                    #y diffusion
                    Rd_y[i, j] = Sy_yx[i, j] + Sy_yy[i, j]                                                                        
    
        Rx = Rc_x + Rd_x
        Ry = Rc_y + Rd_y
        
        #analytical convection terms for v and u momentum
        uconv = -np.exp(-2*tf)*np.cos(x_xu)*np.sin(y_xu)*(np.exp(-2*tf)*np.sin(x_xu)*np.cos(y_xu))
        vconv = np.exp(-2*tf)*np.sin(x_xu)*np.sin(y_xu)*(-np.exp(-2*tf)*np.sin(y_xu)*np.cos(x_xu))
        
        #update time 
        tf += dt
        
        #analytical solution at current time step
        for i in range(1,Nxc+1):
            for j in range(1,Nxc+1):
                u_a[i, j] = -np.exp(-2*tf)*np.cos(x_xu[i])*np.sin(y_xu[j])
                v_a[i, j] = np.exp(-2*tf)*np.sin(x_yv[i])*np.cos(y_yv[j])
        
        

        #Explicit time advancement 
        time_advancement = 'RK1'
        U = time_advance_explicit(U, Rx, Nxc, time_advancement = time_advancement, dt = dt)
        #V = time_advance_explicit(V, Ry, Nxc, time_advancement = time_advancement, dt = dt)
        
        #Semi Implicit time advancement 
        #U = time_advance_semi_implicit(U, Rc_x, Rd_x, Nxc, time_advancement = time_advancement, dt = dt)
        #V = time_advance_semi_implicit(V, Rc_y, Rd_x, Nxc, time_advancement = time_advancement, dt = dt)
        #keyboard()
    
        #copying of BC's for u*
        U[1:Nxc+1, 0] = U[1:Nxc+1, Nxc] 
        U[1:Nxc+1, Nxc+1] = U[1:Nxc+1,1]
        U[0,0:Nxc+2] = U[Nxc, 0:Nxc+2]
        U[Nxc + 1, 0:Nxc+2] = U[1, 0:Nxc+2]
    
        #copying of BC's for v*
        V[1:Nxc+1, 0] = V[1:Nxc+1, Nxc]
        V[1:Nxc+1, Nxc+1] = V[1:Nxc+1,1]
        V[0,0:Nxc+2] = V[Nxc, 0:Nxc+2]
        V[Nxc + 1, 0:Nxc+2] = V[1, 0:Nxc+2]
    
        vel = np.sqrt(U**2 +V**2)
        
        # divergence of velocity
        for i in xrange(1,Nxc+1):
            for j in xrange(1,Nxc+1):
                delu[i, j] = (U[i, j] - U[i, j-1])/dxc[1]
                delv[i, j] = (V[i, j] - V[i-1, j])/dyc[1]
        divV = (delu + delv)/dt
        divV_m = divV[j_C,i_C]     
    
    
        # pressure poisson solver    
        P = spysparselinalg.spsolve(DivGrad.tocsc(),divV_m)
        P_final = np.zeros((Nxc + 1, Nyc + 1))
        P_final[j_C,i_C] = P
    
    
        # correcting pressure gradient
        for i in xrange(1,Nxc):
            for j in xrange(1,Nxc):
                U_c[i, j] = U[i, j] - dt * (P_final[i, j + 1] - P_final[i, j]) / dxc[1]
                V_c[i, j] = V[i, j] - dt * (P_final[i + 1, j] - P_final[i, j]) / dyc[1] 
    
        n += 1
        U = U_c
        V = V_c
        error = np.sqrt((np.sum(np.abs(U - u_a) ** 2)) / Np)
        
        
        print '{} {}'.format(n, error)
        plt.plot(error, Nxc, marker = 'o')
        #print t
        #print(U_c)
        if tf >= T:
            break
    
plt.show()
plt.close()    
keyboard()



