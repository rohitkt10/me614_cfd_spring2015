import os
import sys
import numpy as np
import scipy.sparse as scysparse
from pdb import set_trace as keyboard
from time import sleep
import spatial_operators
import scipy.sparse as scysparse
import scipy.sparse.linalg as spysparselinalg  # sparse linear algebra
import scipy.linalg as scylinalg               # non-sparse linear algebra
import pylab as plt
from matplotlib import rc as matplotlibrc
from datetime import datetime

machine_epsilon = np.finfo(float).eps
pi = np.pi

#define path where plots are to be saved 
figure_folder = "../report/figures/"

if not os.path.exists(figure_folder):
    os.makedirs(figure_folder)


#Define analytical functions
def get_phi2(n, X, Y):
    return ( - 8 * (pi ** 2) * (n ** 2) * np.sin(2 * pi * n * X) * np.sin(2 * pi * n * Y))

#Switches
spy_laplacian = True
w_opt = True
plot_residual = True
non_uniform_grid = True

def sor(DivGrad, Np, q1, w):
    """
    Routine to implement the SOR iteration scheme 
    """
    L = scysparse.tril(DivGrad, k = -1)
    U = scysparse.triu(DivGrad, k = 1)
    D = DivGrad - L - U
    x = np.zeros(Np)
    r_it = np.zeros(6000)
    epsilon = 10 ** (-10)
    ratio = 1.0
    i = 0
    while ratio > epsilon:
        if i >= 6000:
            break
        A = -((w - 1) * D + w * U) * x + w * q1
        x = spysparselinalg.spsolve( D + w * L , A)
        r = DivGrad * x - q1
        r_it[i] = np.linalg.norm(r)
        r_it0 = r_it[0]
        ratio = (r_it[i] / r_it[0])
        if ratio < 10 ** (-10):
            break
        i = i + 1
    number_stop_iter = i
    w_graph = w
    return number_stop_iter, w_graph


def get_grid(Nxc, Nyc = None):
    """
    A routine to create a pressure grid.
    """
    if Nyc == None:
        Nyc = Nxc
    Np = Nxc * Nyc
    Lx = 1.0
    Ly = 1.0
    xu = np.linspace(0., Lx, Nxc+1)
    yv = np.linspace(0., Ly, Nyc+1)
    dxu0 = np.diff(xu)[0]
    dxuL = np.diff(xu)[-1]
    xu = np.concatenate([[xu[0] - dxu0], xu, [xu[-1] + dxuL]])
    dyv0 = np.diff(yv)[0]
    dyvL = np.diff(yv)[-1]
    yv = np.concatenate([[yv[0] - dyv0], yv, [yv[-1] + dyvL]])
    dxc = np.diff(xu)  
    dyc = np.diff(yv)
    xc = 0.5 * (xu[:-1] + xu[1:]) 
    yc = 0.5 * (yv[:-1] + yv[1:]) 
    [Xc, Yc]   = np.meshgrid(xc, yc)   
    [Dxc, Dyc] = np.meshgrid(dxc, dyc)  
    return([Xc,Yc],[Dxc,Dyc])


B = 0.25

def generate_clusterd_grid(Nxc,Nyc):
    N = Nxc/2
    x_mesh = np.linspace(-2*np.pi,2*np.pi,N)
    
    x_new = (1 +np.tanh(B*(x_mesh)))/(np.tanh(B))
    
    x = x_new + max(x_new) - min(x_new) +0.0001*(x_new)
    x_new1 = np.append(x_new,x)
    dxu0 = np.diff(x_new1)[0]
    dxuL = np.diff(x_new1)[-1]

    x_new2 = np.concatenate([[x_new1[0]-dxu0],x_new1,[x_new1[-1]+dxuL]])
    x_new3 = x_new2 - min(x_new2)
    x_new3 = x_new3/max(x_new3)

    xc,yc = np.meshgrid(x_new3, x_new3)
    plt.plot(xc,yc, 'r.')
    plt.show()
    return(xc,yc)

def generate_grid_spacings(Nxc,Nyc):
    N = Nxc/2
    x_mesh = np.linspace(-2*np.pi,2*np.pi,N)
    
    x_new = (1 +np.tanh(B*(x_mesh)))/(np.tanh(B))
    
    x = x_new + max(x_new) - min(x_new) + 0.1*(x_new)
    x_new1 = np.append(x_new,x)
    dxu0 = np.diff(x_new1)[0]
    dxuL = np.diff(x_new1)[-1]

    x_new2 = np.concatenate([[x_new1[0]-dxu0],x_new1,[x_new1[-1]+dxuL]])
    x_new3 = x_new2 - min(x_new2)
    x_new3 = x_new3/max(x_new3)
    dxc = np.diff(x_new3)
    dyc = np.diff(x_new3)
    Dxc,Dyc = np.meshgrid(dxc, dyc)

    return(Dxc,Dyc)




if spy_laplacian:
    # number of (pressure) cells = mass conservation cells
    Nxc  = 4
    Nyc  = 4
    Np   = Nxc * Nyc
    Lx   = 2. * pi
    Ly   = 2. * pi

   
    #non-uniform grid
    t_u = np.linspace(0.,1.0,Nxc+1)
    xu =  t_u*t_u*Lx #x-coordinates
    yv = t_u*t_u*Ly  #y-coordinates 

    # creating ghost cells
    dxu0 = np.diff(xu)[0]
    dxuL = np.diff(xu)[-1]
    xu = np.concatenate([[xu[0]-dxu0],xu,[xu[-1]+dxuL]])
    
    #differences 
    dyv0 = np.diff(yv)[0]
    dyvL = np.diff(yv)[-1]
    yv = np.concatenate([[yv[0]-dyv0],yv,[yv[-1]+dyvL]])
    dxc = np.diff(xu)  
    dyc = np.diff(yv)
    
    #pressure cell center locations
    xc = 0.5*(xu[:-1]+xu[1:])  
    yc = 0.5*(yv[:-1]+yv[1:])  
    
    #Pressure grid mesh
    [Xc,Yc]   = np.meshgrid(xc,yc)  
    [Dxc,Dyc] = np.meshgrid(dxc,dyc)   

    # Pre-allocated at all False = no fluid points
    pressureCells_Mask = np.zeros(Xc.shape)
    pressureCells_Mask[1:-1,1:-1] = True

    # number of actual pressure cells
    Np = len(np.where(pressureCells_Mask==True)[0])

    #Create operators and print time needed
    n_a = datetime.now()
    DivGrad_Neumann = spatial_operators.create_DivGrad_operator(Dxc,Dyc,Xc,Yc,pressureCells_Mask,boundary_conditions="Homogeneous Neumann")
    n_b = datetime.now()
    print 'The time required to compute DivGrad operator with Neumann BC is:' + str((n_b-n_a).total_seconds())
    DivGrad_Dirichlet = spatial_operators.create_DivGrad_operator(Dxc,Dyc,Xc,Yc,pressureCells_Mask,boundary_conditions="Homogeneous Dirichlet")
    n_c = datetime.now()
    print 'The time required to compute DivGrad operator with Dirichlet BC is:' + str((n_c-n_b).total_seconds())
   
    #plot spy of laplacian operator 
    plt.spy(DivGrad_Dirichlet, marker='o', markersize=5)
    plt.title("$\Delta^2$    for Homogeneous Dirichlet")
    figure_name = "q1_divgrad_dirichlet_non_uniform.pdf"
    plt.savefig(figure_folder + figure_name)
    plt.close()

    plt.spy(DivGrad_Neumann, marker='o', markersize=3)
    plt.title("$\Delta^2$ for Homogeneous Neumann")
    figure_name = "q1_divgrad_neumann_non_uniform.pdf"
    plt.savefig(figure_folder + figure_name)
    plt.close()

if w_opt:
    Lx   = 1.0
    Ly   = 1.0
    Nxc_list  = np.array([ 16 , 28 , 48 , 64])
    boundary_conditions_list = ["Homogeneous Dirichlet" , "Homogeneous Neumann"]

    for d,bc in enumerate(boundary_conditions_list):
        for i in xrange(4):
            Nxc = Nxc_list[i]
            Nyc = Nxc
            Np = Nxc * Nyc
            [Xc,Yc], [Dxc,Dyc] = get_grid(Nxc)
            pressureCells_Mask = np.zeros(Xc.shape)
            pressureCells_Mask[1:-1,1:-1] = True
            j_C,i_C = np.where(pressureCells_Mask==True)
            DivGrad = spatial_operators.create_DivGrad_operator(Dxc,
                                                                Dyc,
                                                                Xc,
                                                                Yc,
                                                                pressureCells_Mask,
                                                                boundary_conditions = bc)
            
            #define the set of wave numbers 
            n_set = np.array([2., Nxc/2 , Nxc - 2])
            for wave_number in xrange(3):
                n = n_set[wave_number]
                q1 = get_phi2( n, Xc[j_C,i_C], Yc[j_C,i_C])
                w_list = np.linspace(1.6, 2.0, 41)
                nit = np.zeros(len(w_list))
                w_value = np.zeros(len(w_list))
                for k in xrange(len(w_list)):
                    w = w_list[k]
                    nit[k],w_value[k] = sor(DivGrad, Np, q1, w)
                
                plt.plot(w_value, nit, linewidth = 3, label = '$\omega$ vs number of iterations')
                plt.title('grid_' +repr(Nxc) + ', n_' +repr(n) + ',bc_' +repr(bc))
                figure_name = 'q1_grid_' +repr(Nxc) + ', n_' +repr(n) + ',bc_' +repr(bc)+'.pdf'
                plt.savefig(figure_folder + figure_name)
                plt.close()
        #exit()

if plot_residual:
    Lx   = 1.0
    Ly   = 1.0
    Nxc = 28
    Nyc = 28
    Np = Nxc * Nyc
    [Xc, Yc], [Dxc, Dyc] = get_grid(Nxc)
    pressureCells_Mask = np.zeros(Xc.shape)
    pressureCells_Mask[1:-1,1:-1] = True
    j_C, i_C = np.where(pressureCells_Mask == True)
    boundary_conditions_list = ["Homogeneous Dirichlet" , "Homogeneous Neumann"]
    for d, bc in enumerate(boundary_conditions_list):
        
        DivGrad = spatial_operators.create_DivGrad_operator(Dxc,Dyc,Xc,Yc,pressureCells_Mask,boundary_conditions = bc)
        n = 1
        q1 = get_phi2( n, Xc[j_C,i_C], Yc[j_C,i_C])
        w = 1.7
        #Dirichlet:
        L = scysparse.tril(DivGrad, k = -1)
        U = scysparse.triu(DivGrad, k = 1)
        D = DivGrad - L - U
        x = np.zeros(Np)
        r_it = np.zeros(1000)
        array_x = np.zeros(1000)
        for i in xrange(1000):
            
            A = -((w - 1)*D + w*U)*x + w*q1
            x = spysparselinalg.spsolve( D + w*L , A)
            r = DivGrad*x - q1
            r_it[i] = np.linalg.norm(r)
            array_x[i] = i

        plt.plot(array_x, r_it, linewidth = 3, label = 'Residual vs iteration number')
        plt.title('residual_history_bc_' +repr(bc))
        figure_name = 'q1_residual_history_bc_' +repr(bc) +'.pdf'
        plt.savefig(figure_folder + figure_name)
        plt.close()



if Non_uniform_grid:
    Lx   = 1.0
    Ly   = 1.0
    Nxc = 28
    Nyc = 28
    ohm = 1.8
    Np = Nxc*Nyc
    b_c = "Homogeneous Dirichlet"
    n = 1


    xu = np.linspace(0.,Lx,Nxc+1)
    yv = np.linspace(0.,Ly,Nyc+1)
    
    # creating ghost cells
    dxu0 = np.diff(xu)[0]
    dxuL = np.diff(xu)[-1]
    xu = np.concatenate([[xu[0]-dxu0],xu,[xu[-1]+dxuL]])
    dyv0 = np.diff(yv)[0]
    dyvL = np.diff(yv)[-1]
    yv = np.concatenate([[yv[0]-dyv0],yv,[yv[-1]+dyvL]])
    dxc = np.diff(xu)  # pressure-cells spacings
    dyc = np.diff(yv)

    xc = 0.5*(xu[:-1]+xu[1:])  # total of Nxc cells
    yc = 0.5*(yv[:-1]+yv[1:])  # total of Nyc cells

    [Xc,Yc]   = grid_cluster.generate_clusterd_grid(Nxc,Nyc)     
    [Dxc,Dyc] = grid_cluster.generate_grid_spacings(Nxc,Nyc)

    pressureCells_Mask = np.zeros(Xc.shape)
    pressureCells_Mask[1:-1,1:-1] = True
    
    j_C,i_C = np.where(pressureCells_Mask==True)

    DivGrad = spatial_operators.create_DivGrad_operator(Dxc,Dyc,Xc,Yc,pressureCells_Mask,boundary_conditions = b_c )
    q1 = get_phi2( n, Xc[j_C,i_C], Yc[j_C,i_C])
    f = np.sin(2*np.pi*n*Xc[j_C,i_C])*np.sin(2*np.pi*n*Yc[j_C,i_C])
    L = scysparse.tril(DivGrad, k = -1)
    U = scysparse.triu(DivGrad, k = 1)
    D = DivGrad - L - U
    x = np.zeros(Np)
    res = np.zeros(6000)
    array_x = np.zeros(6000)
    iter_number = np.zeros(6000)
    ratio = np.zeros(6000)
    for i in xrange(6000):
        A = -((ohm - 1)*D + ohm*U)*x + ohm*q1
        x = spysparselinalg.spsolve( D + ohm*L , A)
        r = DivGrad*x - q1
        iter_number[i] = i
        res[i] = np.linalg.norm(r)
        res0 = res[0]
        ratio[i] = (res[i]/res[0])
        print ratio[i]
        if ratio[i] < 10**(-10):
            print 'condition acheived'
            break

    # pouring flattened solution back in 2D array
    Phi = np.zeros((Nyc+2,Nxc+2))*np.nan # remember ghost cells

    Phi[np.where(pressureCells_Mask==True)] = x

    figwidth       = 10
    figheight      = 6
    lineWidth      = 4
    textFontSize   = 28
    gcafontSize    = 30
    
    # Plot solution
    fig = plt.figure(0, figsize=(figwidth,figheight))
    ax   = fig.add_axes([0.15,0.15,0.8,0.8])
    plt.axes(ax)
    plt.plot(Xc,Yc, marker='o',markerfacecolor='black', markersize=4)
    plt.contourf(Xc,Yc,Phi)
    plt.colorbar()

    ax.grid('on',which='both')
    plt.setp(ax.get_xticklabels(),fontsize=gcafontSize)
    plt.setp(ax.get_yticklabels(),fontsize=gcafontSize)
    ax.set_xlabel(r"x",fontsize=textFontSize)
    ax.set_ylabel(r"y",fontsize=textFontSize,rotation=90)
    plt.axis("tight")
    plt.axis("equal")
    fig_name = 'contours with clustered grid.png'
    plt.savefig(figure_path + fig_name)
    plt.close()
    plt.show()